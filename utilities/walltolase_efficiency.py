'''
    Created on September 21, 2022

    @author: Patrick.Cavanagh

    The purpose of this script is to show in real time the efficiency of a laser from wall to lase. BK9115 is polled along with SmartHead server to compare.
'''
import pyvisa
from instruments import BK9115, SmartHead
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import datetime as dt
import random

sh = SmartHead.SmartHeadClient()
ps = BK9115.BK9115(reset=0, verbose=0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = []
ys = []
yc = []
efficiency = -1


def animate(i, xs, ys, yc):
    efficiency, current, voltage = calc_efficiency()
    xs.append(dt.datetime.now().strftime('%H:%M:%S.%f'))
    ys.append(efficiency)
    yc.append(current)

    # Limit x and y lists to 20 items
    xs = xs[-20:]
    ys = ys[-20:]
    yc = yc[-20:]

    # Draw x and y lists
    ax.clear()
    ax.plot(xs, ys, yc)

    # Format plot
    # plt.xticks(rotation=45, ha='right')
    plt.xticks([])
    plt.subplots_adjust(bottom=0.30)
    plt.title('Efficiency over Time')
    plt.ylabel('DC to Lase Efficiency (%)')
    plt.ylim((0, 20))


def calc_efficiency():
    global efficiency
    current = ps.get_current()
    voltage = ps.get_voltage()
    sh_time, sh_power = sh.get_values()
    print(sh_power)
    if sh_power:
        sh_power_float = float(sh_power[0])
        try:
            efficiency = (sh_power_float / (current * voltage)) * 100
        except ZeroDivisionError:
            efficiency = 0
    print(efficiency, current, voltage, sh_power_float, sh_power)

    return efficiency, current, voltage


def main():
    random.seed(1010)
    ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys, yc), interval=100)
    plt.show()


if __name__ == "__main__":
    main()
