import numpy as np
from matplotlib import pyplot as plt

f_gate = 166                    # 166 Hz
dc_gate = .83                   # 83%
t_gate = 1/f_gate

f_pwm = 20e3                    # 20 kHz
dc_pwm = .50                   # 50%
t_pwm = 1/f_pwm


n_pulses = 10
t_step = 1000e-9
s_rate= 1/t_step
t_end = t_gate*n_pulses
n_steps = int(t_end/t_step)

gate_waveform = np.zeros(n_steps)

n_gate = int(t_gate/t_step)
inc_gate = int(n_gate*dc_gate)

print(len(gate_waveform))
for i in range(n_pulses):
    gate_waveform[i*n_gate:i*n_gate+inc_gate]=1
    

pwm_waveform = np.zeros(n_steps)

n_pwm = int(t_pwm/t_step)
inc_pwm = int(n_pwm*dc_pwm)


n_pulses = int(t_end/t_pwm)
print(len(pwm_waveform))
for i in range(n_pulses):
    pwm_waveform[i*n_pwm:i*n_pwm+inc_pwm]=1

waveform = pwm_waveform*gate_waveform
out_str=''
for n in waveform:
    out_str = out_str + str(int(n)) + ','
print(out_str[0:100])
f = open('mqt_20kHz_50percent_166Hz_83percent.txt','w')
f.write(out_str)
f.close()
plt.plot(waveform)
plt.show()
