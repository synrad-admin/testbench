from instruments import SmartHead, BK9115
import time
from multiprocessing import Process
import os
import sys

def log_data(t = 30, save_dir = 'data', save_name = 'tmp.csv'):
    filename = save_dir + '\\' + save_name
    #print("did this print?", os.getppid())
    #sys.stdout.flush()
    sh = SmartHead.SmartHead()
    sh.start_stream()
    ps = BK9115.BK9115(reset = 0)
    #ps.set_output_state("ON")
    f = open(filename,'w', encoding='utf-8')
    f.write("time,SH Timestamp,Power (W),Current(A),Voltage (V)\n")
    log_time0 = 0
    sh_time0 = 0
    
    for i in range(30):
        power, sh_time = sh.get_values()
        if log_time0==0:
            log_time0 = time.time()
            sh_time0 = sh_time[0] 
        current = ps.get_current()
        voltage = ps.get_voltage()
        log_time= time.time()
        for j in range(len(power)):
            result_line = "%.3f,%.3f,%.3f,%.3f,%.3f\n" % (log_time-log_time0, (sh_time[j]-sh_time0)/1000.0, power[j], current, voltage)
            f.write(result_line)
        
        time.sleep(1)
    f.close()
    sh.delete()

"""
if __name__ == '__main__':
    p = Process(target=log_data, args=())
    p.start()
    p.join()
"""
