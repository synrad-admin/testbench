#
#   Hello World client in Python
#   Connects REQ socket to tcp://localhost:5555
#   Sends "Hello" to server, expects "World" back
#

from instruments import SmartHead

shc = SmartHead.SmartHeadClient()
shc.get_values()
