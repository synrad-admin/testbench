#
#   Hello World server in Python
#   Binds REP socket to tcp://*:5555
#   Expects b"Hello" from client, replies with b"World"
#

import time
import zmq
from instruments import SmartHead

sh = SmartHead.SmartHeadServer()
sh.start_stream()
"""
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

while True:
    #  Wait for next request from client
    message = socket.recv()
    sh_power, sh_time = sh.get_values()

    #time.sleep(.25)

    sh_power_str = ''
    sh_time_str = ''
    
    for i in range(len(sh_power)):
        sh_power_str = sh_power_str + ',' + str(sh_power[i])
        sh_time_str = sh_time_str + ',' + str(sh_time[i])

    msg_str = sh_time_str + ';' + sh_power_str
    print(msg_str)
    socket.send_string("%s" % msg_str)
    
    #  Send reply back to client
    #socket.send(b"%f,%f" % (power, sh_time))
"""
