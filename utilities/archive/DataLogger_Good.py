from instruments import NIUSB6000, BK9115
import time
from multiprocessing import Process
import os
import sys

def log_data(t = 30, save_dir = 'data', save_name = 'tmp.csv', q = '', t_inc = .25):
    filename = save_dir + '\\' + save_name
    #print("did this print?", os.getppid())
    #sys.stdout.flush()
    daq = NIUSB6000.NIUSB6000()
    ps = BK9115.BK9115(reset = 0)
    #ps.set_output_state("ON")
    f = open(filename,'w', encoding='utf-8')
    f.write("time,Power (W),Power (V), Current(A),Voltage (V)\n")
    log_time0 = 0
    
    #for i in range(30):
    while q.empty():
        power_v=  daq.read_value()
        power_w = power_v * 44.2622950819672
        if log_time0==0:
            log_time0 = time.time() 
        current = ps.get_current()
        voltage = ps.get_voltage()
        log_time = time.time()
        result_line = "%.3f,%.3f,%.3f,%.3f,%.3f\n" % (log_time-log_time0, power_w, power_v, current, voltage)
        f.write(result_line)
        time.sleep(t_inc)
    f.close()

"""
if __name__ == '__main__':
    p = Process(target=log_data, args=())
    p.start()
    p.join()
"""
