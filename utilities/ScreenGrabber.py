import pathlib
from datetime import datetime
from instruments import MDO3034
import pyvisa

test_name = '1-28-2022_Waveforms'
data_dir = 'C:\\Development\\testbench\\data'  # file path to save data to

# Create results directories
save_dir_stub = data_dir + '\\' + test_name + '\\'
pathlib.Path(save_dir_stub + 'ScreenCaptures').mkdir(parents=True, exist_ok=True)

# Instantiate MDO3034
oscope = MDO3034.MDO3034(reset=1)
capture_dir = save_dir_stub + 'Screencaptures\\'

# Determine if and where an image label should be placed
label_loc = [[-1, -1], [0, 0], [300, 0], [600, 0], [0, 443], [650, 443]]
try:
    label_loc_index = int(input("Please Select an Image Label Location:\n"
                                "====================\n"
                                "| 1*      2      3 |\n"
                                "|                  |                 *Indicates a Best Choice Location for Label\n"
                                "|                  |\n"
                                "| 4*             5 |\n"
                                "====================\n"
                                "No Label: 0 (or anything else)\n"
                                ">> "))
except ValueError:
    label_loc_index = 0

print("Note: Screenshot will be captured immediately after a filename is given. Chose filename 'q' to exit program.")
while True:
    date_str = datetime.today().strftime('%Y_%m_%d_%H_%M_%S')
    instance = input("Capture Label: ")
    if instance.lower() == 'q':
        break
    png_name = instance + '__%s.png' % date_str
    try:
        if label_loc_index != 0:
            oscope.show_annotation(state=True)
        oscope.set_annotation(instance, int(label_loc[label_loc_index][0]), int(label_loc[label_loc_index][1])) # TODO Dynamically center labels?
        oscope.save_screen_image(preview=0, save_dir=capture_dir, save_name=png_name)
        print("Image saved as {} to {}".format(png_name, capture_dir))
        oscope.show_annotation(state=False)
    except pyvisa.errors.VisaIOError:  # TODO Move this check to the save_screen_image function when VC is set up on lab
        print(
            "Error! Cannot save temporary image to E:/Temp.png. Please make sure an external drive is attached to the MDO3034.")
        break
