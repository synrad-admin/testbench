import time
import pathlib
from tests.lst_rise_fall.lst_rise_fall import rise_fall
from tests.lst_cold_mark.lst_cold_mark import cold_mark
from tests.lst_power_linearity.lst_power_linearity import power_linearity

def main():
    phase = 'debug'
    dut = 'test_unit'
    data_dir = 'C:\\Users\\aaron.scott\\Development\\testbench\\data'

    # Create Test Information, Save Directories, etc.
    pathlib.Path(data_dir + '\\' + phase).mkdir(parents=True, exist_ok=True)
    test_info = (data_dir, phase, dut)

    ############################################################################
    # BEGIN TESTS
    ############################################################################

    # Rise Fall Testing
    #rise_fall(test_info,'lst_rise_fall_1khz_10percent.cfg')
    #rise_fall(test_info,'lst_rise_fall_100hz_10percent.cfg')

    # Cold Mark Testing
    #cold_mark(test_info,'mqt_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    #cold_mark(test_info,'mqt_20kHz_10percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    #cold_mark(test_info,'mqt_20kHz_20percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    #cold_mark(test_info,'mqt_20kHz_30percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    #cold_mark(test_info,'mqt_20kHz_40percent_166Hz_83percent.cfg', phase=phase, dut=dut)

    # Marking Quality Testing

    # Power Linearity Testing
    power_linearity(test_info,'mqt_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)

    # Modulation Testing

    # Peak Power Testing

    # Power of time testing
    

if __name__ == "__main__":
    main()
