import nidaqmx
import time

class NIUSB6000():
    def __init__(self, verbose=1, reset=0):
        self.name = "AG33155b"
        self.task = nidaqmx.Task()
        self.task.ai_channels.add_ai_voltage_chan("Dev3/ai0")

    def read_value(self):
        value = self.task.read()
        return value

"""
daq = NIUSB6000()
while True:
    print(daq.read_value())
    time.sleep(.2)
"""
