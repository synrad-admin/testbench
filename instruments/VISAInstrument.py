'''
Created on May 5, 2020

@author: Aaron.Scott
'''
import pyvisa
from datetime import datetime
import time

'''
Group of dictionaries that ties known id_str to their human readable form
'''
device_name = {
    "MDO3034": "?*::0x0699::0x0408::?*",
    "MSOX3024": None,
    "BK9115": "?*::0xFFFF::0x9115::?*",
    "AG33511B": "?*::0x0957::0x2707::?*"
}


class VISAInstrument(object):
    """Base class for VISA instruments. This class provides functions used by VISA-compliant instruments using SCPI
        commands. SCPI common commands are implemented in this base class while instrument-specific commands are
        implemented in various sub-classes. For information on pyvisa, see
        https://pyvisa.readthedocs.io/en/latest/index.html
        FOr information on common SCPI commands, see:
        https://rfmw.em.keysight.com/wireless/helpfiles/n5106a/scpi_commands_common.htm

    Attributes:
        inst (pyvisa.resources.Resource): Instance of a pyvisa resource. pyvisa allows for any number of specific
            resource types. Only pyvisa.resources.usb.USBInstrument tested so far. For information on the specific
            resource types, see https://pyvisa.readthedocs.io/en/latest/api/resources.html#
        verbose (int): Verbose setting. 0 will not print logging information, 1 will.
        error (int): Error status. 0 means no error present, 1 means error present.
        name (str): Name of instrument. This will be the name that will be printed in the logging functions.
        dtfmtstr (str): Date format string used by datetime module. This will format the date and time string used in
            the logging functions. For more information, see: https://docs.python.org/3/library/datetime.html

    """

    def __init__(self, id_str, name, verbose=1, reset=1):
        """initialization function for VISAInstrument.

        Args:
            id_str (str): Unique identifier for VISA resource. A USB instrument, for example, has the format:
                USB[board]::manufacturer ID::model code::serial number[::USB interface number][::INSTR]
                Resource name documentation can be found here: https://pyvisa.readthedocs.io/en/1.8/names.html
                Another valuable resource for resource names for the NI-VISA library can be found here:
                https://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/findingvisaresourcesusingregularexpressions/
            name (str): The human readable name of the instrument to be used for things like logging.
            verbose (int): Verbose setting. 0 will not print logging information, 1 will.
            reset (int): If 1, the instrument will reset upon be initialized. If 0, it will not.

        """
        rm = pyvisa.ResourceManager()
        inst_list = rm.list_resources(id_str)
        self.inst = rm.open_resource(inst_list[0])
        self.verbose = verbose
        self.error = 0
        self.name = name
        self.dtfmtstr = "%m%d%Y %H:%M:%S"
        if reset:
            self.rst()
            time.sleep(1)

    def write(self, message):
        """Writes SCPI message to instrument

        Args:
          message (str): A SCPI command (not query). No response is expected

        Returns:
          nothing
        """
        date_time = datetime.strftime(datetime.now(), self.dtfmtstr)
        if self.verbose:
            print("%s  %-8s  %s" % (date_time, self.name, message))
        self.inst.write(message)

    def query(self, message):
        """Writes SCPI message to instrument and reads response.

        Args:
          message (str): A SCPI command (not query). For SCPI query commands, a '?' is placed at the end of the message.

        Returns:
          response (str): Response from the instrument.
        """
        date_time = datetime.strftime(datetime.now(), self.dtfmtstr)
        if self.verbose:
            print(date_time + "  " + self.name + "\t" + message + ' ', end='')
        response = self.inst.query(message)
        if self.verbose:
            print(response[:-1])
        return response

    def set_verbose(self, verbose):
        """Setter method for verbose

        Args:
          verbose: 0 to set verbose off, 1 to set verbose on

        Returns:
          Nothing
        """
        self.verbose = verbose

    def idn(self):
        """SCPI common command to identify instrument

        Args:
          none

        Returns:
          id_str (str): Response from the instrument.
        """
        message = "*IDN?"
        if self.verbose:
            print(message)
        id_str = self.query(message)
        return id_str

    def opc(self, query=1):
        """SCPI common command to get/set Standard Event Status Register.

         Args:
           query (int): if 1, gets the Standard Event Status Register when all pending operations have finished. If 0,
                queries for the status of it.

         Returns:
           opc_bit (int): Standard Event Status Register bit if query=1, nothing if query=0
         """
        if query:
            message = "*OPC?"
        else:
            message = "*OPC"

        if query:
            opc_bit = int(self.query(message))
        else:
            opc_bit = self.write(message)
        return opc_bit

    def rst(self):
        """SCPI common command to reset instrument.

         Args:
           none

         Returns:
           nothing
         """
        message = "*RST"
        return self.write(message)

    def set_timeout(self, amount=5000):
        """Most VISA I/O operations may be performed with a timeout. If a timeout is set, every operation that
        takes longer than the timeout is aborted and an exception is raised. Timeouts are given per instrument in
        milliseconds. For more information, see:
        https://pyvisa.readthedocs.io/en/1.8/resources.html

         Args:
           amount (int): instrument timeout in milliseconds

         Returns:
           nothing
         """
        self.inst.timeout = amount

    def self_test(self):
        """Sends the TST? command to the instrument instructing it to complete an internal self test.

        Returns:
            Returns 0 if all tests pass, returns 1 if one or more tests fails.
        """
        return self.query("*TST?")

    def wait(self):
        """causes the instrument to wait until all pending commands are completed, before executing any other commands,
        depending on flow of test scripts may not ever be needed.
        Returns:
            Nothing
        """
        self.write("*WAI")
