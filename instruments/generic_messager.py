'''
    Created on December 22, 2021

    @author: Patrick.Cavanagh

    The purpose of this script is for general debugging of VISA enabled equipment using raw SCPI communications. This
    script sets up pyvisa and presents the user with a field to enter in SCPI commands without any extra ceremony.
'''
import VISAInstrument
import pyvisa

SCRIPT_VERSION = '1.0'


def main():
    print("VISA-SCPI Generic Messager Script Version: {}".format(SCRIPT_VERSION))
    # Grab the list of instruments connected
    rm = pyvisa.ResourceManager()
    inst_list = rm.list_resources()

    # Display them to the user in a readable list, the user then selects a device numerically
    print("Detected Instruments: ")
    for i in range(len(inst_list)):
        # Detect consistent ASLR devices and note they are most likely not instruments
        if str(inst_list[i]).find("ASRL") != -1:
            print("**", end='')
        print("{}. {}".format(i + 1, inst_list[i]))

    print("\n**Most likely not an instrument and rather an existing serial device from the PC. Only connect to this "
          "device if you are certain it is the one you are looking for.\n\n")
    while True:
        selection = int(input("Which device would you like to communicate with? (Select with list number)\n"))-1 # human readable off by one stuff
        try:
            # Retrieve the required id field from the full name, ie remove USBx and INSTR, and replace with wildcards
            device_id = str(inst_list[selection]).split(sep="::")[1:3]
            device_id = "?*::" + "::".join(device_id) + "::?*"
            inst = VISAInstrument.VISAInstrument(device_id, "DUT")
            break
        except ValueError:
            print("Invalid Selection.")
        except IndexError:
            print("Selection out of range, please select a number between 1 and {}".format(len(inst_list)))

    # Script enters into a loop communicating with the instrument
    print("Connecting to {}, enter 'q' to quit or 'help' for a list of available commands".format(inst_list[selection]))
    while True:
        cmd = input("[cmd] : ")
        print(cmd)
        try:
            if cmd.lower() == 'q':
                print("Terminating connection.")
                break

            # In the future this could be fully fleshed out into a help prompt, for now its fairly basic
            elif cmd.lower() == 'help':
                print("VISA Generic Messager Script Version: {}".format(SCRIPT_VERSION))
                print("\nSCPI Command Syntax\n<Header> <Argument>,<Argument N>...\nHeader: Command to be "
                      "run\nArgument: Argument for the command\nArgument N: Optional Nth Argument for the command\n")
                print("This script facilitates a quick SCPI communication link with an instrument using SCPI "
                      "commands. For a list of available SCPI commands please refer to the Programmer's Guide for the "
                      "device you are working with. For a list of universal commands and what they do, please see "
                      "below. All SCPI communications are either a write, read, or query (which is just shorthand for a write then a read). This script will automatically detect if you are attempting to write or query based on the presence of a '?' in the command, all SCPI query commands have one.\nTo perform a read command on the instrument, type in 'read'.\n")
                print("IEEE 488.2 Common Commands should work on any device independent of their manufacturer, "
                      "an incomplete list of commands follows.\n "
                      "*CLS\tThe Clear Status (CLS) command clears the status byte by emptying the error queue and "
                      "clearing all the event registers including the Data Questionable Event Register, the Standard "
                      "Event Status Register, the Standard Operation Status Register and any other registers that are "
                      "summarized in the status byte.")
            elif cmd.lower() == 'read':
                print("[Read] : {}".format('Not currently implemented.'))
            elif cmd[-1] == '?':
                print("[DUT] : {}".format(inst.query(cmd)))
            else:
                inst.write(cmd)
        except pyvisa.errors.VisaIOError:
            print("\n[ERROR] : VisaIOError raised due to operation timeout, did you misspell the command?")
        except IndexError:
            print("[ERROR] : Empty command.")


if __name__ == "__main__":
    main()
