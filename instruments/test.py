import pyvisa
# from VISAInstrument import VISAInstrument
import MDO3034
import configparser

# idstr = "?*::0x0699::0x0408::?*"
# name = "MDO3034"
# scope = VI.VISAInstrument(idstr, name)
# scope.write("*RST")
# scope.rst()
# scope.query("*IDN?")
# scope.query("HORizontal:SCAle?")
# scope.write("HORizontal:SCAle 2E-6")

cfg = configparser.ConfigParser()
print(cfg.read('test.cfg'))

scope = MDO3034.MDO3034(reset=1)
scope.set_normal_edge_trigger(1, .1)
scope.set_ch_termination(1, "FIFty")
scope.set_channel_label(1, "Hello World!")
scope.set_annotation('Howdy!', 100, 100)
scope.show_annotation()
scope.save_screen_image("./", preview=1)

x = 0
y = 0
while True:
    x += 1
    y += 1

    if x > 1023:
        x = 0
    if y > 767:
        y = 0

    scope.set_annotation_location(x, y)
    scope.opc(query=1)


# scope.get_curve_data()
# MDO3034.curve_to_xy('./')
