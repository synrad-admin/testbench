'''
Created on May 6, 2020- this is the signal generator

@author: Aaron.Scott
'''
from instruments.VISAInstrument import VISAInstrument
import numpy as np
import time

class AG33511b(VISAInstrument):
    '''
    classdocs
    '''


    def __init__(self, verbose=1, reset=0):
        '''
        Constructor
        '''
       
        idstr = "?*::0x0957::0x2707::?*"
        name = "AG33511b"
        super().__init__(idstr, name, verbose=verbose, reset=reset)

    def write(self, message):
        super().write(message)
        #print(self.inst.query("SYSTEM:ERROR?"))
        #input("Press any key...")
        
    def err(self):
        self.query("SYSTEM:ERROR?")

    def output_load(self, load="INF"):
        self.write(":OUTPut:LOAD %s" % load)
        
    def load_arb_wf(self, data, volt = 2.5, srate=400000):
        self.write("SOURce1:DATA:VOLatile:CLEar")
        self.write("FORM:BORD NORM")
        self.inst.write_binary_values("SOURce1:DATA:ARBitrary:DAC testbench, ", np.array(data)*32767, datatype='h', is_big_endian=True)
        self.write("SOURce1:FUNCtion:ARBitrary1 testbench")
        self.write("SOURce1:FUNCtion ARB")
        self.write("SOURCE1:FUNCtion:ARB:SRATe %i" % srate)
        self.write("SOURCE1:VOLT %.3E" % volt)
        self.write("SOURCE1:VOLT:OFFSET 0")
        self.write("OUTPUT1:LOAD INF")
        
    def trigger(self):
        self.write("*TRG")
        
    def set_single_shot(self, cycles=1, trigger="BUS"):
        self.write('BURST:STAT ON')
        self.write('BURST:MODE TRIGGERED')
        self.write('BURST:NCYCLES %i' % cycles)
        self.write('TRIG:SOURCE %s' % trigger)

    def set_pulse_burst(self, freq = 5e3, duty = .1, cycles=1000, trigger="BUS"):
        self.write("OUTPUT1 OFF")
        self.write(":OUTPUT:LOAD INF")
        self.write('BURST:STAT ON')
        self.write('BURST:NCYCLES %i' % cycles)
        self.write("SOURCE1:FUNCTION PULSE")
        #self.write("FUNCTION:PULSE:DCYCLE 0")
        #self.write("SOURCE1:FUNCTION:PULSE:PERIOD 2E-4")
        self.write("SOURCE1:FUNCTION:PULSE:PERIOD %f" % (1/freq))
        self.write("FUNCTION:PULSE:DCYCLE %f" % float(duty*100))
        #self.write("OUTPUT1:LOAD INF")
        self.write("SOURCE1:VOLT 7")
        self.write("SOURCE1:VOLT:OFFSET 3.5")
        self.write("BURS:MODE TRIG")
        self.write("TRIG:SOUR BUS")
        self.write("OUTPUT1 ON")
        self.write("*TRG")
        time.sleep(.5)

    def set_continuous_pwm(self, freq = 5e3, duty = .1, cycles=1000, trigger="BUS"):
        self.write("OUTPUT1 OFF")
        self.write(":OUTPUT:LOAD INF")
        self.write('BURST:STAT OFF')
        self.write("SOURCE1:FUNCTION SQUARE")
        self.write("SOURCE1:FUNCTION:SQUARE:PERIOD %f" % (1/freq))
        self.write("FUNCTION:SQUARE:DCYCLE %f" % float(duty*100))
        #self.write("OUTPUT1:LOAD INF")
        self.write("SOURCE1:VOLT 7")
        self.write("SOURCE1:VOLT:OFFSET 3.5")

    def set_pwm_freq_dc(self, freq = -1, dc = -1):
        if (freq >= 0):
            self.write("SOURCE1:FUNCTION:SQUARE:PERIOD %.5E" % (1/freq))
        if (dc >= 0):
            self.write("SOURCE1:FUNCTION:SQUARE:DCYCLE %.5E" % (dc))
        return 0

    def set_output(self, state='OFF'):
        self.write("OUTPUT %s" % state)

    def set_arb_wf(self, data_str):
        self.write(":OUTPUT OFF")
        self.write(":OUTPUT:LOAD INF")
        self.write("SOURCE:FUNCTION ARB")
        self.write("SOURCE:FUNCTION:ARBITRARY:SRATE 1000000")
        self.write(":SOURce:DATA:ARBitrary MYWAVE,%s" % data_str)
        self.write(":MMEMory:STORe:DATA \"MYWAVE\"")
        self.write(":MMEMory:LOAD:DATA \"MYWAVE\"")
        self.write(":SOURce:FUNCtion:ARBitrary \"MYWAVE\"")
        self.write(":SOURce:FUNCtion:ARBitrary:Filter OFF")
        self.write("BURS:STAT ON")
        self.write("BURS:MODE TRIG")
        self.write("TRIG:SOUR BUS")
        self.write(":SOURCE:VOLT:HIGH 5.0")
        self.write(":OUTPut ON")
        
        time.sleep(.5)
        self.write("*TRG")

"""       
awg = AG33511b(reset=1)
awg.set_continuous_pwm()
awg.set_output("ON")
for dcycle in range(2,20,2):
    for frequency in range(1000, 20000, 1000):
        #print(dcycle, frequency)
        awg.set_pwm_freq_dc(frequency, dcycle)
        time.sleep(.25)
awg.set_output("OFF")
"""
        

        


        
        
        
        
