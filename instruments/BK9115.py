'''
Created on May 5, 2020

@author: Aaron.Scott
'''
from instruments.VISAInstrument import VISAInstrument

class BK9115(VISAInstrument):
    '''
    A class representing the BK Precision 9115 power supply.
    
    Programmer manual available here: https://bkpmedia.s3.amazonaws.com/downloads/programming_manuals/en-us/9115_series_programming_manual.pdf
    BK9115 is a subclass of VISAInstrument
    
    Attributes:
        idstr (str): A string containing the manufacturer and part id
    '''


    def __init__(self, verbose=1, reset=1):
        """Initializes BK9115 with idstr"""
        idstr = "?*::0xFFFF::0x9115::?*"
        name = "BK9115"
        super().__init__(idstr, name, reset=reset)
        
    def set_voltage(self, v):
        """Sets output voltage"""
        self.write("SOURCE:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE %i" % int(v))
        self.opc()
    
    def get_voltage(self):
        response = self.query("MEASURE:SCALAR:VOLTAGE?")
        return float(response[:-1])
        
    def set_current(self, i):
        self.write("SOURCE:CURRENT:LEVEL:IMMEDIATE:AMPLITUDE %i" % int(i))
        self.opc()
        
    def get_current(self):
        response = self.query("MEASURE:SCALAR:CURRENT?")
        return float(response[:-1])
        
    def set_output_state(self, state):
        self.write("OUTPUT:STATE %s" % state)
        self.opc()
        
    def get_output_state(self):
        return self.query("OUTPUT:STATE?")
        

    
