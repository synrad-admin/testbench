#import win32com.client #Commented out temporarily
import time
import zmq

class SmartHead():
    """
    classdocs
    """

    def __init__(self, verbose=1, reset=0):
        
        self.ophirApp = win32com.client.Dispatch('OphirLMMeasurement.CoLMMeasurement')
        self.SerialNumbers = self.ophirApp.ScanUSB()
        self.h_USB = self.ophirApp.OpenUSBDevice(self.SerialNumbers[0]);
        (Sensor_SN, Sensor_Type, Sensor_Name)= self.ophirApp.GetSensorInfo(self.h_USB,0);
        
    def start_stream(self):
        self.ophirApp.StartStream(self.h_USB,0);
        time.sleep(.5)
        
    def get_values(self):
        (Value, Timestamp, Status)= self.ophirApp.GetData(self.h_USB,0);
        return Value, Timestamp

class SmartHeadServer():
    """
    classdocs
    """

    def __init__(self, verbose=1, reset=0):
        
        self.ophirApp = win32com.client.Dispatch('OphirLMMeasurement.CoLMMeasurement')
        self.SerialNumbers = self.ophirApp.ScanUSB()
        self.h_USB = self.ophirApp.OpenUSBDevice(self.SerialNumbers[0]);
        (Sensor_SN, Sensor_Type, Sensor_Name)= self.ophirApp.GetSensorInfo(self.h_USB,0);
        
    def start_stream(self):
        self.ophirApp.StartStream(self.h_USB,0);
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://*:5555")

        while True:
            #  Wait for next request from client
            message = socket.recv()
            sh_power, sh_time = self.read_values()

            sh_power_str = ''
            sh_time_str = ''
            
            for i in range(len(sh_power)):
                sh_power_str = sh_power_str + ',' + str(sh_power[i])
                sh_time_str = sh_time_str + ',' + str(sh_time[i])

            msg_str = sh_time_str + ';' + sh_power_str
            socket.send_string("%s" % msg_str)

    def read_values(self):
        (Value, Timestamp, Status)= self.ophirApp.GetData(self.h_USB,0);
        return Value, Timestamp


class SmartHeadClient():
    """
    classdocs
    """

    def __init__(self, verbose=1, reset=0):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect("tcp://localhost:5555")
        
    def get_values(self):
        # print('do i get here?')
        self.socket.send(b"Hello")
        message = self.socket.recv_string()
        (sh_time, sh_power) = message.split(';')
        return sh_time.split(',')[1:], sh_power.split(',')[1:]
