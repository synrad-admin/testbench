'''
Created on May 5, 2020- this subclass is for the oscilloscope

@author: Aaron.Scott
'''
from instruments.VISAInstrument import VISAInstrument
import numpy as np
import re
from matplotlib import pyplot as plt
import matplotlib.image as mpimg


# import time

class MDO3034(VISAInstrument):
    '''
    classdocs
    '''

    def __init__(self, verbose=1, reset=0):
        '''
        Constructor
        '''
        """Initializes MDO3034 with idstr"""
        idstr = "?*::0x0699::0x0408::?*"
        name = "MDO3034"
        super().__init__(idstr, name, verbose=verbose, reset=reset)

    def set_normal_edge_trigger(self, ch, threshold, direction="RISE"):
        # set trigger type
        self.write(":SELECT:CH%i 1" % int(ch))
        self.write(":TRIGGER:A:TYPE EDGE")
        self.write(":TRIGGER:A:EDGE:SOURCE CH%i" % int(ch))
        self.write(":TRIGGER:A:LOWERTHRESHOLD:CH%i %.3E" % (ch, threshold))
        self.write(":TRIGGER:A:MODE NORMAL")
        self.write(":TRIGGER:A:EDGE:SLOPE %s" % direction)
        self.write("ACQUIRE:STOPAFTER RUNSTOP")
        self.write("ACQUIRE:STATE ON")

    def set_ch_termination(self, ch, term):
        self.write("CH%i:TERMINATION %s" % (ch, term))

    def set_ch_scale(self, ch, scale):
        self.write("CH%i:SCALE %f" % (ch, scale))

    def set_ch_position(self, ch, position):
        self.write("CH%i:POSITION %f" % (ch, position))

    def set_ch_bandwidth(self, ch, bandwidth):
        self.write("CH%i:BANDWIDTH %f" % (ch, bandwidth))

    def set_horizontal_scale(self, scale):
        self.write("HORIZONTAL:SCALE %.3E" % scale)

    def set_horizontal_record_length(self, length):
        self.write("HORIZONTAL:RECORDLENGTH %i" % length)

    def set_horizontal_delay_time(self, delay):
        self.write("HORIZONTAL:DELAY:TIME %.3E" % delay)

    def set_single_shot(self):
        self.write("ACQUIRE:STOPAFTER SEQUENCE")

    def display_channel(self, ch, val):
        message = "SELECT:CH%i %s" % (ch, val)
        self.write(message)

    def set_channel_label(self, ch, label):
        message = "CH%i:LABel '%s'" % (ch, label)
        self.write(message)

    def show_annotation(self, state=True):
        '''

        Args:
            state: True to show the annotation, False to hide it. Defaults to True

        Returns:
            Nothing
        '''
        if state:
            message = 'MESSage:STATE ON'
        else:
            message = 'MESSage:STATE OFF'
        self.write(message)

    def set_annotation(self, text, x, y):
        """

        Args:
            text: Message to display on the screen
            x: x coordinate of top left corner from 0 to 1023
            y: y coordinate of top left corner from 0 to 767

        Returns:
            Nothing
        """
        message = "MESSage:SHOW '{text}'".format(text=text)
        self.write(message)
        message = "MESSage:BOX {x},{y}".format(x=x, y=y)
        self.write(message)

    def set_annotation_location(self, x, y):
        self.write("MESSage:BOX {x},{y}".format(x=x, y=y))

    def get_curve_data(self, source=1, start=1, stop=10000, filename='tmp.dat'):
        self.write(":VERBOSE ON")
        self.write(":DATA:SOURCE CH%i" % source)
        self.write(":DATA:START %i" % start)
        self.write(":DATA:STOP %i" % stop)
        self.write(":WFMOUTPRE:ENCDG ASCII")
        self.write(":WFMOUTPRE:BYT_NR 1")
        self.write(":HEADER 1")
        waveformpre = self.query(":WFMOUTPRE?")
        curve = self.inst.query(":CURVE?")
        f = open(filename, 'w')
        f.write(waveformpre)
        f.write(curve)
        f.close()
        # log.info(waveformpre)
        return (waveformpre, curve)

    def save_screen_image(self, save_dir='data', save_name='tmp.png', preview=0):
        self.opc()
        # https://www.tek.com/support/faqs/how-save-and-transfer-screenshot-4-5-or-6-series-mso
        self.write('SAVE:IMAGe \"E:/Temp.png\"')
        self.inst.query('*OPC?')
        self.inst.write('FILESystem:READFile \"E:/Temp.png\"')
        imgData = self.inst.read_raw(1024 * 1024)
        self.inst.query('*OPC?')
        # Save image data to local disk
        filename = save_dir + save_name
        file = open(filename, "wb")
        file.write(imgData)
        file.close()
        if preview:
            img = mpimg.imread(filename)
            plt.imshow(img)
            plt.show(block=False)
            plt.pause(3)
            plt.close()
            # plt.show()

    def cursor_function(self, function):
        # SCREEN|WAVEform|OFF
        self.write('CURSor:FUNCtion {}'.format(function))

    def cursor_source(self, source):
        if source > 4 or source < 1:
            return False
        self.write('CURSor:SOUrce CH{}'.format(source))

    def get_cursor_vbars(self):
        return self.query('CURSor:VBArs?')

    def set_cursor_position(self, position, cursor=1):
        '''
        Args:
            cursor: 1 or 2
            position: NR3 (Float with Exponent, ie 5.012E-5)
        '''
        if cursor > 2 or cursor < 1:
            return False
        self.write('CURSor:VBArs:POSITION{} {}'.format(cursor, position))

    def get_cursor_hval(self, cursor=1):
        '''
        Args:
            cursor: 1 or 2
        Returns: False if invalid cursor, float if valid
        '''
        if cursor > 2 or cursor < 1:
            return False
        self.write('CURSor:VBArs:HPOS{}?'.format(cursor))

    def set_measurement_source(self, source='CH1'):
        self.write('MEASUrement:IMMed:SOUrce1 {}'.format(source))

    def set_measurement_type(self, mtype):
        # {AMPlitude|AREa|BURst|CARea|CMEan|CRMs|DELay|FALL|FREQuency
        # |HIGH|HITS|LOW|MAXimum|MEAN|MEDian|MINImum|NDUty|NEDGECount
        # |NOVershoot|NPULSECount|NWIdth|PEAKHits|PEDGECount|PDUty
        # |PERIod|PHAse|PK2Pk|POVershoot|PPULSECount|PWIdth|RISe|RMS
        # |SIGMA1|SIGMA2|SIGMA3|STDdev|TOVershoot|WAVEFORMS}
        self.write('MEASUrement:IMMed:TYPe {}'.format(mtype))

    def get_measurement(self):
        return self.query('MEASUrement:IMMed:VALue?')

    def set_measurement_gating(self, gating):
        """OFF|SCREen|CURSor"""
        self.write('MEASUrement:GATing {}'.format(gating))

    def measure_rise_between_points(self, start_time, end_time, ch=4):
        '''
        Args:
            start_time: NR3 data
            end_time: NR3 data
            ch: 1-4
        Returns: rise time
        '''
        self.cursor_function('WAVEform')
        self.cursor_source(ch)
        self.set_cursor_position(start_time, cursor=1)
        self.set_cursor_position(end_time, cursor=2)
        self.set_measurement_source()
        self.set_measurement_type('RISe')
        self.set_measurement_gating('CURSor')

        rise_time = self.get_measurement()

        return rise_time


def curve_to_xy(save_dir='data', save_name='tmp.csv', filename='tmp.dat', preview=1):
    f = open(filename)
    waveformpre = f.readline()
    curve = f.readline()
    f.close()

    xincr = find_param("XINCR", waveformpre)
    nr_pt = find_param("NR_PT", waveformpre)
    ymult = find_param("YMULT", waveformpre)
    yoff = find_param("YOFF", waveformpre)
    #     yzero = find_param("YZERO", waveformpre)

    x = np.arange(nr_pt) * xincr
    y = []
    for i in curve[7:-1].split(','):
        # y.append(float(i)*float(ymult)+float(yoff))
        y.append(float(i) * float(ymult) - float(yoff) * float(ymult))
    if preview:
        plt.plot(x, y)
        plt.show()
    np.savetxt(save_dir + save_name, np.vstack((np.array(x), np.array(y))).T, delimiter=', ')
    return (np.array(x), np.array(y))


def find_param(param, w):
    numeric_const_pattern = '[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?'
    rx = re.compile(param + '\ ' + numeric_const_pattern, re.VERBOSE)
    res = rx.findall(w)[0]
    val = res[len(param) + 1:]
    return float(val)
