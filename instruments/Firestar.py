# %% Import modules
import nidaqmx as daq
import time

# INPUTS
REMOTE_RESET_REQUEST = 0
REMOTE_INTERLOCK_REQUEST = 1
SHUTTER_OPEN_REQUEST = 2

# OUTPUTS
INTERLOCK_OPEN_OUT = 0
FAULT_DETECTED_OUT = 1
LASER_READY_OUT = 2
SHUTTER_OPEN_OUT = 3
LASER_ACTIVE_OUT = None


class Firestar():
    def __init__(self):
        self.task_out = daq.Task()
        self.task_in = daq.Task()
        self.task_out.do_channels.add_do_chan("Dev1/port1/line0:3")
        self.task_in.di_channels.add_di_chan("Dev1/port0/line0:3")
        self.val = 1
        self.task_out.write(self.val)
        
    def reset(self):
        self.write(REMOTE_RESET_REQUEST, 0)
        self.write(REMOTE_RESET_REQUEST, 1)

    def write(self, bit_pos, val):
        self.val = set_bit(self.val, bit_pos, val) 
        self.task_out.write(self.val)

    def read(self):
        in_vals = self.task_in.read()
        return bin(in_vals)[2:].zfill(4)[::-1]

    def get_inputs(self):
        return self.val

    def print_inputs(self):
        return "{0:b}".format(self.val).zfill(3)[::-1]

    def print_inputs2(self):
        input_str = self.print_inputs()
        output_str = self.read()

        output_msg =  "*"*40
        output_msg = output_msg + "\nINPUTS:\n"
        output_msg = output_msg + "Remote Reset Request:\t\t%s\n" % (input_str[REMOTE_RESET_REQUEST])
        output_msg = output_msg + "Remote Interlock Request:\t%s\n" % (input_str[REMOTE_INTERLOCK_REQUEST])
        output_msg = output_msg + "Shutter Open Request:\t\t%s\n\n" % (input_str[SHUTTER_OPEN_REQUEST])

        output_msg = output_msg + "OUTPUTS:\n"
        output_msg = output_msg + "Interlock Open Output:\t\t%s\n" % (output_str[INTERLOCK_OPEN_OUT])
        output_msg = output_msg + "Fault Detected Output:\t\t%s\n" % (output_str[FAULT_DETECTED_OUT])
        output_msg = output_msg + "Laser Ready Output:\t\t%s\n" % (output_str[LASER_READY_OUT])
        output_msg = output_msg + "Shutter Open Output:\t\t%s\n" % (output_str[SHUTTER_OPEN_OUT])
        output_msg = output_msg + "Laser Active Output:\t\t0\n"

        output_msg = output_msg + "*"*40 +'\n'
        #print(output_msg)
        return output_msg
        
    #def print_outputs(self):
        #return "{0:b}".format(self.val)
    


def set_bit(v, index, x):
    mask = 1 << index   # Compute mask, an integer with just bit 'index' set.
    v &= ~mask          # Clear the bit indicated by the mask (if x is False)
    if x:
        v |= mask         # If x was True, set the bit indicated by the mask.
    return v            # Return the result, we're done.
"""        
fs = Firestar()

t_sleep=.5

while True:
    fs.write(SHUTTER_OPEN_REQUEST,1)
    print(fs.print_inputs2())
    time.sleep(t_sleep)
    fs.write(SHUTTER_OPEN_REQUEST,0)
    print(fs.print_inputs2())
    time.sleep(t_sleep)
"""

