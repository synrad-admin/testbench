import BDS2PyAPI, BDS2PyDefs

bds=BDS2PyAPI.BDS2PyAPI()
res=BDS2PyDefs.BDS2StdMeasDataStruct()

bds.bds2_loadBDS2DLL()

bds.bds2_connect(
    BDS2_index = 0,
    host_name = '192.168.0.151')

bds.bds2_setStartScan(0)
bds.bds2_getStdNumMeasurements(0,res)
print(res)
