/**

@verbatim

The MIT License (MIT)

Copyright (c) 2021 Bird

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

@endverbatim

*/

/*
@file BirdBDS2API.h.

Copyright (c) 2021 Bird
@author Afif Bouhenni
*/

#pragma once

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the BirdBDS2API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// BirdBDS2API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef BirdBDS2API_EXPORTS
#define BirdBDS2API  //extern "C" __declspec(dllexport)
#else
#define BirdBDS2API  extern "C" __declspec(dllimport)
#endif

/** DLL calling convention used. */
#define DLL_CALLING __stdcall

#define BDS2_MAX_ERROR_MES_SIZE 256

#define FIRST_USER_ERROR 0x02000000		         // from MSDN bit 29 set for application specific errors
										
enum message {
		e_bds_BIRD_COPYRIGHT=FIRST_USER_ERROR,
		e_bds_bcerrUnknown,						 // Unknown error - host or client did not set an error code.          0x0001
		e_bds_bcerrSuccess,						 // No error - successful operation.
        e_bds_bcerrNotConnected,				 // Client not connected to host.
        e_bds_bcerrConnectionRejected,			 // Client connection rejected by host.
        e_bds_bcerrUnknownMsg,					 // Unknown message ID received.
        e_bds_bcerrUnknownConn,					 // Unknown connection instance ID received.
        e_bds_bcerrUnknownService,				 // Unknown service code received.
        e_bds_bcerrUnknownClass,				 // Unknown class ID received.                                         0x0008
        e_bds_bcerrUnknownInst,					 // Unknown object instance number received.
        e_bds_bcerrUnknownAttrib,				 // Unknown class attribute or method ID.
        e_bds_bcerrInvalidLength,				 // Invalid packet length.
        e_bds_bcerrParse,						 // Invalid packet data received - unable to parse.
        e_bds_bcerrCorruptedData,				 // Corrupted data section (invalid data integrity value).
        e_bds_bcerrAccess,						 // Client does not have proper access rights.
        e_bds_bcerrCmdNotSent,					 // Received a response for a command that was not sent.
        e_bds_bcerrInvalidParam,				 // Invalid parameter value (out-of-range).                            0x0010
        e_bds_bcerrInsufficientBuffer,			 // Not enough room to store request data.
        e_bds_bcerrUnsupportedProtocol,			 // Client protocol is not supported by host.
        e_bds_bcerrFatal,						 // Fatal error occurred internal to the host or client.
        e_bds_bcerrInsufficientMemory,			 // Not enough memory to complete allocation command.
        e_bds_bcerrServiceNotImplemented,		 // The service code is known but not implemented.
        e_bds_bcerrServiceNotAllowed,			 // The service code is known but not allowed.
        e_bds_bcerrUnidentifiedClient,			 // The client did not identify itself to the host.
        e_bds_bcerrDataNotAvailable,			 // No data available for data poll request.                           0x0018
        e_bds_bcerrInterruptOutOfSynch,			 // Interrupt out-of-sync.
        e_bds_bcerrTimeout,						 // Operation time-out.
		e_bds_bcerrCommunicationFailure,		 // Low-level communications failure (i.e. socket read/write error).
        e_bds_bcerrIO =FIRST_USER_ERROR+33,		 // I/O file read/write error.                                         0x0021
        e_bds_bcerrFeatureUnavailable,			 // Unsupported feature for a specific model.

		e_bds_berrApiStart =FIRST_USER_ERROR+40, // Not an error.                                                      0x0028				
		e_bds_bcerrInvalidArgument,              // Invalid function call argument.
		e_bds_bcerrInvalidHandle,				 // Invalid DLL handle.                            
		
		e_bds_bcerrInvalidAGC,					 // Invalid AGC setting.
		e_bds_bcerrInvalidAttenIdB,				 // Invalid current attenuation.
		e_bds_bcerrInvalidAttenVdB,				 // Invalid voltage attenuation.

		e_bds_bcerrInvalidAttenResetDelay,		 // Invalid attenuation reset delay.

		e_bds_bcerrInvalidNumFunds,			     // Invalid number of fundamentals.

		e_bds_bcerrFund1FreqOutofRange,			 // Fundamental 1 frequency out of range (300000 to 253000000).        0x0030
		e_bds_bcerrFund2FreqOutofRange,			 // Fundamental 2 frequency out of range (300000 to 253000000).
		e_bds_bcerrFund3FreqOutofRange,			 // Fundamental 3 frequency out of range (300000 to 253000000).

		e_bds_bcerrInvalidFund1Harm1,			 // Fundamental 1 invalid harmonic 1
		e_bds_bcerrInvalidFund1Harm2,			 // Fundamental 1 invalid harmonic 2
		e_bds_bcerrInvalidFund1Harm3,			 // Fundamental 1 invalid harmonic 3
		e_bds_bcerrInvalidFund1Harm4,			 // Fundamental 1 invalid harmonic 4

		e_bds_bcerrInvalidFund2Harm1,			 // Fundamental 2 invalid harmonic 1
		e_bds_bcerrInvalidFund2Harm2,			 // Fundamental 2 invalid harmonic 2                                   0x0038
		e_bds_bcerrInvalidFund2Harm3,			 // Fundamental 2 invalid harmonic 3
		e_bds_bcerrInvalidFund2Harm4,			 // Fundamental 2 invalid harmonic 4
				
		e_bds_bcerrInvalidFund3Harm1,			 // Fundamental 3 invalid harmonic 1
		e_bds_bcerrInvalidFund3Harm2,			 // Fundamental 3 invalid harmonic 2
		e_bds_bcerrInvalidFund3Harm3,			 // Fundamental 3 invalid harmonic 3
		e_bds_bcerrInvalidFund3Harm4,			 // Fundamental 3 invalid harmonic 4
	
		e_bds_bcerrFund1Harm1FreqOutofRange,	 // Fundamental 1 harmonic 1 frequency out of range
		e_bds_bcerrFund1Harm2FreqOutofRange,	 // Fundamental 1 harmonic 2 frequency out of range                    0x0040
		e_bds_bcerrFund1Harm3FreqOutofRange,	 // Fundamental 1 harmonic 3 frequency out of range
		e_bds_bcerrFund1Harm4FreqOutofRange,	 // Fundamental 1 harmonic 4 frequency out of range

		e_bds_bcerrFund2Harm1FreqOutofRange,	 // Fundamental 2 harmonic 1 frequency out of range
		e_bds_bcerrFund2Harm2FreqOutofRange,	 // Fundamental 2 harmonic 2 frequency out of range
		e_bds_bcerrFund2Harm3FreqOutofRange,	 // Fundamental 2 harmonic 3 frequency out of range
		e_bds_bcerrFund2Harm4FreqOutofRange,	 // Fundamental 2 harmonic 4 frequency out of range
	
		e_bds_bcerrFund3Harm1FreqOutofRange,	 // Fundamental 3 harmonic 1 frequency out of range
		e_bds_bcerrFund3Harm2FreqOutofRange,	 // Fundamental 3 harmonic 2 frequency out of range                    0x0048
		e_bds_bcerrFund3Harm3FreqOutofRange,	 // Fundamental 3 harmonic 3 frequency out of range
		e_bds_bcerrFund3Harm4FreqOutofRange,	 // Fundamental 3 harmonic 4 frequency out of range
	
		e_bds_bcerrInvalidFund1IMDFund2,		 // Fundamental 1 invalid secondary fundamental for IMD
		e_bds_bcerrInvalidFund2IMDFund2,		 // Fundamental 2 invalid secondary fundamental for IMD
		e_bds_bcerrInvalidFund3IMDFund2,		 // Fundamental 3 invalid secondary fundamental for IMD

		e_bds_bcerrInvalidFund1IMDSel,			 // Fundamental 1 invalid IMD selection
		e_bds_bcerrInvalidFund2IMDSel,			 // Fundamental 2 invalid IMD selection
		e_bds_bcerrInvalidFund3IMDSel,			 // Fundamental 3 invalid IMD selection                                0x0050

		e_bds_bcerrFund1IMDFreqOutofRange,		 // Fundamental 1 IMD frequency out of range
		e_bds_bcerrFund2IMDFreqOutofRange,		 // Fundamental 2 IMD frequency out of range
		e_bds_bcerrFund3IMDFreqOutofRange,		 // Fundamental 3 IMD frequency out of range

		e_bds_bcerrInvalidFund1DFilter,			 // Fundamental 1 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)
		e_bds_bcerrInvalidFund2DFilter,			 // Fundamental 2 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)
		e_bds_bcerrInvalidFund3DFilter,			 // Fundamental 3 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)

		e_bds_bcerrInvalidFund1MeasMode,		 // Fundamental 1 invalid measurement mode (CW, Pulse, Multi-Level)
		e_bds_bcerrInvalidFund2MeasMode,		 // Fundamental 2 invalid measurement mode (CW, Pulse, Multi-Level)    0x0058
		e_bds_bcerrInvalidFund3MeasMode,		 // Fundamental 3 invalid measurement mode (CW, Pulse, Multi-Level)

		e_bds_bcerrInvalidFund1TrkMode,			 // Fundamental 1 invalid tracking mode (Fixed or Tracking)
		e_bds_bcerrInvalidFund2TrkMode,			 // Fundamental 2 invalid tracking mode (Fixed or Tracking)
		e_bds_bcerrInvalidFund3TrkMode,			 // Fundamental 3 invalid tracking mode (Fixed or Tracking)

		e_bds_bcerrInvalidFund1viTrk,			 // Fundamental 1 invalid signal tracking (V, I, or Auto)
		e_bds_bcerrInvalidFund2viTrk,			 // Fundamental 2 invalid signal tracking (V, I, or Auto)
		e_bds_bcerrInvalidFund3viTrk,			 // Fundamental 3 invalid signal tracking (V, I, or Auto)

		e_bds_bcerrFund1iNoiseLvlOutofRange,	 // Fundamental 1 current noise level out of range                     0x0060
		e_bds_bcerrFund2iNoiseLvlOutofRange,	 // Fundamental 2 current noise level out of range
		e_bds_bcerrFund3iNoiseLvlOutofRange,	 // Fundamental 3 current noise level out of range
		
		e_bds_bcerrFund1vNoiseLvlOutofRange,	 // Fundamental 1 voltage noise level out of range
		e_bds_bcerrFund2vNoiseLvlOutofRange,	 // Fundamental 2 voltage noise level out of range
		e_bds_bcerrFund3vNoiseLvlOutofRange,	 // Fundamental 3 voltage noise level out of range

		e_bds_bcerrFund1State1FreqOutofRange,	 // ML Fundamental 1 State 1 seed frequency out of range
		e_bds_bcerrFund1State2FreqOutofRange,	 // ML Fundamental 1 State 2 seed frequency out of range
		e_bds_bcerrFund1State3FreqOutofRange,	 // ML Fundamental 1 State 3 seed frequency out of range               0x0068
		e_bds_bcerrFund1State4FreqOutofRange,	 // ML Fundamental 1 State 4 seed frequency out of range

		e_bds_bcerrFund2State1FreqOutofRange,	 // ML Fundamental 2 State 1 seed frequency out of range
		e_bds_bcerrFund2State2FreqOutofRange,	 // ML Fundamental 2 State 2 seed frequency out of range
		e_bds_bcerrFund2State3FreqOutofRange,	 // ML Fundamental 2 State 3 seed frequency out of range
		e_bds_bcerrFund2State4FreqOutofRange,	 // ML Fundamental 2 State 4 seed frequency out of range

		e_bds_bcerrFund3State1FreqOutofRange,	 // ML Fundamental 3 State 1 seed frequency out of range
		e_bds_bcerrFund3State2FreqOutofRange,	 // ML Fundamental 3 State 2 seed frequency out of range
		e_bds_bcerrFund3State3FreqOutofRange,	 // ML Fundamental 3 State 3 seed frequency out of range               0x0070
		e_bds_bcerrFund3State4FreqOutofRange,	 // ML Fundamental 3 State 4 seed frequency out of range

		e_bds_bcerrInvalidNumTrkStates,			 // ML invalid number of states.

		e_bds_bcerrTrkState1DelayTooSmall,		 // ML State 1 delay too small for the selected detection filter
		e_bds_bcerrTrkState2DelayTooSmall,		 // ML State 2 delay too small for the selected detection filter
		e_bds_bcerrTrkState3DelayTooSmall,		 // ML State 3 delay too small for the selected detection filter
		e_bds_bcerrTrkState4DelayTooSmall,		 // ML State 4 delay too small for the selected detection filter

		e_bds_bcerrTrkState1DelayTooBig,		 // ML State 1 delay too big for the selected detection filter
		e_bds_bcerrTrkState2DelayTooBig,		 // ML State 2 delay too big for the selected detection filter         0x0078
		e_bds_bcerrTrkState3DelayTooBig,		 // ML State 3 delay too big for the selected detection filter
		e_bds_bcerrTrkState4DelayTooBig,		 // ML State 3 delay too big for the selected detection filter

		e_bds_bcerrTrkState1DurationTooShort,	 // ML State 1 duration too short for the selected detection filter
		e_bds_bcerrTrkState2DurationTooShort,	 // ML State 2 duration too short for the selected detection filter
		e_bds_bcerrTrkState3DurationTooShort,	 // ML State 3 duration too short for the selected detection filter
		e_bds_bcerrTrkState4DurationTooShort,	 // ML State 4 duration too short for the selected detection filter

		e_bds_bcerrTrkState1DurationTooLong,	 // ML State 1 duration too long for the selected detection filter
		e_bds_bcerrTrkState2DurationTooLong,	 // ML State 2 duration too long for the selected detection filter     0x0080
		e_bds_bcerrTrkState3DurationTooLong,	 // ML State 3 duration too long for the selected detection filter
		e_bds_bcerrTrkState4DurationTooLong,	 // ML State 4 duration too long for the selected detection filter
		
		e_bds_bcerrPulsePeriodTooShort,			 // ML Pulse period too short
		e_bds_bcerrPulsePeriodOutOfRange,		 // ML Pulse period out of range
		e_bds_bcerrInvalidComponentId,		     // Invalid component ID
		e_bds_bcerrInvalidNumFreqComponents,     // Invalid Number of frequency components

		e_bds_bcerrPulseTriggerDelayOutofRange,  // Pulse rate delay out of range.
		e_bds_bcerrDataUpdateRateOutofRange,	 // Data Update Rate out of range.                                     0x0088
		e_bds_bcerrStdConfigNotInitialized,      // Std configuration not initialized
		e_bds_bcerrInvalidAverageWindowSize,	 // Invalid Average window size.
		e_bds_bcerrInvalidNumMultiStatesComponents, // Invalid Number of Multi-states frequency components

		e_bds_bcerrLast							 // <-- Not an error - marks the last in the enum list!
};
typedef enum message message;


typedef struct bdsErrorData bdsErrorData;
struct bdsErrorData
{
	int		ErrorCode;
	char	ErrorMessage[BDS2_MAX_ERROR_MES_SIZE]; 
};

#define BDS2_MAX_CLIENTS 4                     // maximum number of BDS2 receivers
										       // that the API can simultaneously connect to.

#define BDS2_MAX_FUNDAMENTALS 3                // maximum number of fundamentals
#define BDS2_MAX_HARMONICS 5                   // fundamental(1st harmonic) + 4
#define BDS2_MAX_INTERMODS 6

#define BDS2_MAX_STATES 4                      // maximum number of states
#define BDS2_FRAME_MAX_LEN 1000000             // in usecs (1 second)
#define BDS2_FRAME_MIN_LEN 20                  // in usecs (20 microSeconds)
#define BDS2_STATE_DELAY_MAX 500000            // in usecs (0.5 seconds)
#define BDS2_STATE_DELAY_MIN 0                 // in usecs (0 microSeconds) 
#define BDS2_STATE_DUR_MAX 500000              // in usecs (0.5 seconds)
#define BDS2_STATE_DUR_MIN 5                   // in usecs (5 microSeconds) 

#define BDS2_MAX_V_NOISE_LVL 5                 // 5 volts
#define BDS2_MIN_V_NOISE_LVL 0                 // 0 volts
#define BDS2_DEFAULT_V_NOISE_LVL 2.5           // 2.5 volts

#define BDS2_MAX_I_NOISE_LVL .1                // 100 milliamps
#define BDS2_MIN_I_NOISE_LVL 0                 // 0 amps
#define BDS2_DEFAULT_I_NOISE_LVL .03           // 30 milliamps

#define BDS2_PULSE_SYNC_DELAY_MAX 500000       // in usecs (0.5 seconds)
#define BDS2_PULSE_SYNC_DELAY_MIN 0            // in usecs (0 microSeconds)

#define BDS2_STD_MAX_DATA_UPDATE_RATE 600000   // in milliSeconds (10 minutes)
#define BDS2_STD_MIN_DATA_UPDATE_RATE 2        // in milliSeconds (2 milliSeconds)
#define BDS2_STD_MIN_RS232_DATA_UPDATE_RATE 20 // in milliSeconds ()20 milliSeconds)

#define BDS2_MAX_NUM_FREQ_COMPONENTS 12        // 12 frequency components
#define BDS2_MAX_NUM_DATA_SETS 48              // 12 frequency components * BDS2_MAX_STATES
#define BDS2_MAX_NUM_MULTI_STATE_MEAS 20       // Maximum number of multi-states measurements.
/**
	The lowest possible frequency.
*/
#define BDS2_STD_MIN_FREQ 308000

/**
	The highest possible frequency.
*/
#define BDS2_STD_MAX_FREQ 252000000

/**
	The maximum window size for "simple" average mode.
*/
#define BDS2_AVG_MAX_WIN_SIZE_SIMPLE 16

#define BDS2_MAX_DATA_UPDATE_RATE 100 // 100 milliSeconds

////////////////////////////////////////////////////////////////////////


/**
	Gregorian date structure.
	Format:
	date: YYYYMMDD
		YYYY = 4-digit year (0-9999), 
		MM   = 2-digit month (1-12),
		DD   = 2-digit day (1-31)

	To date: date = (year * 10000) + (month * 100) + day
	From date: 
		year = date/10000
		month = (date - (year * 10000))/100
		day = date - (year * 10000) - (month * 100)
*/
typedef unsigned int BDS2_Date; // Packed date (YYYYMMDD).

typedef struct _APIVersionInfo_t
{
	char dllName[256];
	short major;	// Major revision number.
	short minor;	// Minor revision number.
	BDS2_Date date; // Release date.
} DLL_VersionInfo;

typedef struct _BDS2VersionInfo_t
{
	char productName[256];
	short major;	// Major revision number.
	short minor;	// Minor revision number.
	BDS2_Date date; // Release date.
} BDS2_VersionInfo;

typedef struct _BDS2SystemInfo_t
{
	int componentId;		 // BDS2_eComponentId probe, cable, receiver 
	char componentName[128]; // component name probe, cable, receiver
	char model[128];		 // component model 
	char serialNumber[128];  // component serial number 
} BDS2_SystemInfo;

typedef struct
{
	unsigned int date; // Packed date (YYYYMMDD).
	unsigned int time; // Packed time (HHMMSS).
	unsigned int hres; // Packed high resolution time (mmmuuu).
} BDS2_Timestamp;

typedef struct
{
	short year;	    // 4-digit year (0-9999).
	short month;	// 2-digit month (1-12).
	short day;		// 2-digit day (1-31).
	short hours;	// 2-digit hour (0-23).
	short minutes;	// 2-digit minutes (0-59).
	short seconds;	// 2-digit seconds (0-59).
	short millis;	// 3-digit milliSeconds (0-999).
	short usec;	    // 3-digit microSeconds (0-999).
} BDS2_DateAndTime;

typedef enum
{
	e_bds2AGC_OFF=0,	// AGC OFF / manual attenuation setting
	e_bds2AGC_ON=1,		// AGC ON / auto
} BDS2_eautoAGC;

typedef enum
{
	e_bds2Attn0dB=0,		// AGC manual attenuation 0  dB.
	e_bds2Attn6dB=6,		// AGC manual attenuation 6  dB.
	e_bds2Attn12dB=12,		// AGC manual attenuation 12 dB.
	e_bds2Attn18dB=18,		// AGC manual attenuation 18 dB.
	e_bds2Attn24dB=24,		// AGC manual attenuation 24 dB.
	e_bds2Attn30dB=30		// AGC manual attenuation 30 dB.
} BDS2_eManualAttunation;

typedef enum
{
	e_bds2ResetDelay1ms=1,		// AGC attenuation reset delay  1 milliSecond.
	e_bds2ResetDelay2ms=2,		// AGC attenuation reset delay  2 milliSeconds.
	e_bds2ResetDelay4ms=4,		// AGC attenuation reset delay  4 milliSeconds.
	e_bds2ResetDelay10ms=10,	// AGC attenuation reset delay  10 milliSeconds.
	e_bds2ResetDelay20ms=20,	// AGC attenuation reset delay  20 milliSeconds.
	e_bds2ResetDelay40ms=40,	// AGC attenuation reset delay  40 milliSeconds.
	e_bds2ResetDelay100ms=100	// AGC attenuation reset delay  100 milliSeconds.

} BDS2_eResetDelay;

typedef struct _AttnConfig_t
{
	unsigned char isAGCOn;		// Non-zero for automatic gain control ON= 1, OFF =0.
	unsigned char attnVdB;		// Voltage channel attenuation as in BDS2_eManualAttunation
	unsigned char attnIdB;		// Current channel attenuation as in BDS2_eManualAttunation
	unsigned char resetDelayms; // AGC attenuation reset delay in milliSeconds
} BDS2_AttnConfig;


/**
	BDS2 measurement mode.
*/
typedef enum
{
	e_bds2_CWMode=0,		// Std CW Tracking Mode. 
	e_bds2_PulsedMode,		// Std Pulsed Tracking Mode. 
	e_bds2_MultiStateMode	// Std Multi-State Tracking Mode. 
} BDS2_eMeasMode;

/**
	BDS2 tracking mode.
*/
typedef enum
{
	e_bds2_FixedFreq=0, 	// Std Fixed Frequency Mode. 
	e_bds2_TrackingFreq 	// Std Frequency Tracking Mode.
} BDS2_eTrackingMode;

/**
	BDS2 tracking mode.
*/
typedef enum
{
	e_bds2_AutoTracking=0, 	// Std Auto Tracking Mode. 
	e_bds2_VTracking, 		// Std Voltage Tracking Mode. 
	e_bds2_ITracking 		// Std Current Tracking Mode. 
}BDS2_eVITracking;

/**
	BDS2 detection filters.
*/
typedef enum
{
	e_bds2_1MHz=0,  //  1 MHz detection filter.
	e_bds2_100kHz,  //  100 kHz detection filter.   
	e_bds2_10kHz,  	//  10 kHz detection filter. 
	e_bds2_1kHz  	//  1 kHz detection filter.
} BDS2_eDetFilter;


/**
	BDS2 detection filter delays.
*/
typedef enum
{
	e_bds2_1MHzDelay_us=2,    //    2 microSeconds.
	e_bds2_100kHzDelay_us=16, //   16 microSeconds.
	e_bds2_10kHzDelay_us=131, //  131 microSeconds.
	e_bds2_1kHzDelay_us=1048, // 1048 microSeconds.
} BDS2_eDetFilterDelay;

typedef enum
{
	e_bds2_HarmSelOFF= 0,	 // harmonic frequency not selected.
	e_bds2_HarmSelON,		//  harmonic frequency selected.
} BDS2_eHarmonicSel;

/**
	Intermodulation product selections.
	Intermodulation number (value).
	Intermodulation product +/- nF2.
	-3 = F1 - 3F2
	-2 = F1 - 2F2
	-1 = F1 - F2
	0 = No intermodulation.
	1 = F1 + F2
	2 = F1 + 2F2
	3 = F1 + 3F2
*/
typedef enum
{
	e_bds2_IMNone = 0,	  // no intermodulation products.
	e_bds2_IM1Plus1Minus, // 1 "plus" and 1 "minus" product.
	e_bds2_IM2Plus2Minus, // 2 "plus" and 2 "minus" products.
	e_bds2_IM3Plus3Minus  // 3 "plus" and 2 "minus" products.
} BDS2_eIntermodSel;

typedef enum
{
	e_bds2_IMFund2_None= 0, // intermodulation no second fundamental.
	e_bds2_IMFund2_1,		// intermodulation fundamental 1 as second fundamental.
	e_bds2_IMFund2_2,		// intermodulation fundamental 2 as second fundamental.
	e_bds2_IMFund2_3,		// intermodulation fundamental 3 as second fundamental.
} BDS2_eintermodFund2;


/**
	BDS2 Component ID.
*/
typedef enum
{
	e_bds2_probe=0, // probe component ID. 
	e_bds2_cable, 	// cable component ID. 
	e_bds2_receiver // receiver component ID. 
} BDS2_eComponentId;

typedef char BDS_IntermodNum;
typedef struct _DLLFundamental_t
{
	unsigned int  freqHz;						// Fundamental frequency (in Hz).
	unsigned int  harmSel[BDS2_MAX_HARMONICS];  // Harmonic selection.
	unsigned int  intermodFund2;				// F2 fundamental for intermod.
	unsigned int  intermodSel;					// as defined BDS2_eIntermodSel
	unsigned int  detFilter;					// as defined in BDS2_eDetFilter.
	float		  vNoiseLevel;					// Voltage noise floor level. volts.
	float		  iNoiseLevel;					// Current noise floor level. amps.
	unsigned int  viTracking;					// as defined in BDS2_eVITracking.
	unsigned int  measMode;						// as defined in BDS2_eMeasMode.
	unsigned int  trackingMode;					// as defined in BDS2_eTrackingMode.
	unsigned int  stateFreqHz[BDS2_MAX_STATES];	// state measurement frequency.

} BDS2_Fundamental;

typedef struct _DLLMultiStateConfig_t
{
	unsigned int trkStateDelay;				// Std tracking state delay.
	unsigned int trkStateDuration;			// Std tracking state duration.
	unsigned int trkStateNumberOfFrames;	// Std tracking state number of frames.
} BDS2_MultiState;

typedef struct _DLLStdConfig_t
{
	unsigned int numFunds;			// The number of fundamentals.
	unsigned int pulseTriggerDelay;	// Pulse mode trigger delay in microSeconds.
	unsigned int dataUpdateRate;	// Data production rate.
	unsigned int numStates;			// The number of states.
	unsigned int pulsePeriod;		// Pulse period in usecs. 
} BDS2_StdConfig;


typedef struct _DLLMultiStates_t
{
	BDS2_MultiState state[BDS2_MAX_STATES];
} BDS2_MultiStates;

typedef struct _DLLlFundamentals_t
{
	BDS2_Fundamental fund[BDS2_MAX_FUNDAMENTALS];
} BDS2_Fundamentals;


typedef struct _DLLBDS2_AvgExObj_t
{
	unsigned char isAverageEnabled;	// 0 = OFF 1 = ON
	unsigned int averageWindowSize;	// Average size up to 16.
} BDS2_AvgExObj;

typedef struct
{
	unsigned char fnum;  // Fundamental number (1 to 3). 
	unsigned char hnum;  // Harmonic number (1 to 4). 
	unsigned char inum;  // Intermod number (-3 to 3). 
	unsigned char snum;  // State number (1 to 4). 
	unsigned char attnV; // Voltage channel attenuation in dB. 
	unsigned char attnI; // Current channel attenuation in dB. 
	unsigned int freq;   // Measured frequency in Hz. 
	float voltage;       // Voltage magnitude (RMS voltage). 
	float current;       // Current magnitude (RMS current). 
	float phase;		 // Phase measurement between voltage and current (degrees).
	double Zmag;		 // Impedance (magnitude in Ohms)
	double R;			 // Resistance (real part of impedance in Ohms)
	double X;			 // Reactance (imaginary part of impedance in Ohms)
	double delPower;     // Delivered power (Watts). 
	double fwdPower;     // Forward power (Watts). Only displayed if 25 < Zmag < 100 and -20 < Phase < 20.
	double rflPower;     // Reflected power (Watts).  Only displayed if 25 < Zmag < 100 and -20 < Phase < 20.
} BDS2_VIMeas;

typedef enum
{
	e_bds2StatusIdle=0,
	e_bds2StatusAGCOff=1,			// AGC ON/OFF status
	e_bds2StatusVOver=2,			// Voltage overflow
	e_bds2StatusIOver=3,			// Current overflow
	e_bds2StatusProbeDisconnect=4,	// Probe disconnected
	e_bds2StatusNotCal=5,			// Calibration off
	e_bds2StatusArcDetected=6		// Arc detected
} BDS2_eStatus;

typedef struct _DLLStdDatasetHeader_t
{
	unsigned int status;							//  BDS2 System status bit field as defined in BDS2_eStatus.
	unsigned short numMeas;							//  The number of measurements contained.
	BDS2_Timestamp timestamp;						//  Acquisition time stamp.
	unsigned char numAvg[BDS2_MAX_FUNDAMENTALS];	//  Fundamental tracking.
} BDS2_StdDatasetHeader;


#ifdef __cplusplus
extern "C"
{
#endif
/*****************************BDS2DLL info**************/
BirdBDS2API int DLL_CALLING bds2dll_getDLLRevision(DLL_VersionInfo* dllVersion);

/*****************************connection to BDS2**************/
BirdBDS2API int DLL_CALLING bds2dll_connectTCPIP(int BDS2_index, const char* p_host, int timeoutMillis);
BirdBDS2API int DLL_CALLING bds2dll_disconnect(int BDS2_index);

/************************************get BDS2 information******************/
BirdBDS2API int DLL_CALLING bds2dll_getBDS2Revision(int BDS2_index, BDS2_VersionInfo* p_version);
BirdBDS2API int DLL_CALLING bds2dll_setSystemTime(int BDS2_index, BDS2_DateAndTime* p_datetime);
BirdBDS2API int DLL_CALLING bds2dll_getSystemTime(int BDS2_index, BDS2_DateAndTime* p_datetime);
BirdBDS2API int DLL_CALLING bds2dll_getSystemInfo(int BDS2_index, BDS2_SystemInfo* p_systenInfo);

/**************************BDS2 gain setting ***********************/
BirdBDS2API int DLL_CALLING bds2dll_initAttenuation(int BDS2_index, BDS2_AttnConfig* p_attenuation);
BirdBDS2API int DLL_CALLING bds2dll_setAttenuation(int BDS2_index, const BDS2_AttnConfig* attenuation);
BirdBDS2API int DLL_CALLING bds2dll_getAttenuation(int BDS2_index, BDS2_AttnConfig* p_attenuation);

/**************************BDS2 Standard Tracking ***********************/
BirdBDS2API int DLL_CALLING bds2dll_setAvgConfig(int BDS2_index, BDS2_AvgExObj* p_avgCfg);
BirdBDS2API int DLL_CALLING bds2dll_getAvgConfig(int BDS2_index, BDS2_AvgExObj* p_avgCfg);

BirdBDS2API int DLL_CALLING bds2dll_initStdConfig(int BDS2_index, BDS2_StdConfig* p_stdCfg, BDS2_Fundamentals* p_funds, BDS2_MultiStates* p_states);
BirdBDS2API int DLL_CALLING bds2dll_setStdConfig(int BDS2_index, BDS2_StdConfig* p_stdCfg, BDS2_Fundamentals* p_funds, BDS2_MultiStates* p_states);
BirdBDS2API int DLL_CALLING bds2dll_getStdConfig(int BDS2_index, BDS2_StdConfig* p_stdCfg, BDS2_Fundamentals* p_funds, BDS2_MultiStates* p_states);
BirdBDS2API int DLL_CALLING bds2dll_startScan(int BDS2_index);
BirdBDS2API int DLL_CALLING bds2dll_stopScan(int BDS2_index);
BirdBDS2API int DLL_CALLING bds2dll_getStdNumMeasurements(int BDS2_index, int* numMeasurements);
BirdBDS2API int DLL_CALLING bds2dll_getStdSingleDataset(int BDS2_index, BDS2_StdDatasetHeader* p_measHeaders, BDS2_VIMeas* p_measData);

BirdBDS2API int DLL_CALLING bds2dll_getLastError(bdsErrorData* Error);
BirdBDS2API int DLL_CALLING bds2dll_finalize();

#ifdef __cplusplus
}
#endif

