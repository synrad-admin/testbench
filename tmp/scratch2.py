'''
Created on May 7, 2020

@author: Aaron.Scott
'''

import AG33511b
import MDO3034
import BK9115
import time
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from PIL import Image


def pixel_to_duty(pixels=[0, 1, 0, .7, 1, 0], sub_pixels=10):
    n=len(pixels)
    pix_mat = np.zeros((n,sub_pixels),dtype=np.int8)
    for i in range(n):
        pix_mat[i,0:int(pixels[i]*sub_pixels)]=1
    new_pix_mat = np.reshape(pix_mat, n*sub_pixels)
    new_pix_mat[-1]=0
    return new_pix_mat

filename = "Ruler.png"
 
img = Image.open(filename)
#img.thumbnail((1000, 500), Image.ANTIALIAS)  # resizes image in-place
data = np.asarray(img)
dims = data.shape
line = data[int(dims[0]/2),:,0]/255
 
#plt.plot(line)
 
#plt.show()


# Initialize Instruments
wfg = AG33511b.AG33511b()
scope = MDO3034.MDO3034()
ps = BK9115.BK9115()

# Wait for instruments to complete reset
time.sleep(3)

# Begin Testing
rList = np.array([0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0,0,1,0,1,0,1,0,1,0])
rList = pixel_to_duty(line)
print(type(rList))


wfg.load_arb_wf(rList, srate = 2000000)
wfg.write("SOURCE1:FUNCTION:ARB:FILT OFF")
wfg.write("OUTPUT1 ON")
wfg.set_single_shot()


scope.display_channel(1, "OFF")
scope.display_channel(2, "OFF")
scope.display_channel(3, "ON")
scope.display_channel(4, "ON")
scope.set_horizontal_delay_time(160e-6)
scope.set_horizontal_scale(40e-6)
scope.set_ch_scale(3, 1)
scope.set_ch_scale(4, 2)
scope.set_normal_edge_trigger(3, 1)
scope.set_single_shot()
time.sleep(2)
wfg.trigger()
#wfg.opc()
time.sleep(1)
scope.save_screen_image("tmp.png", preview=0)

scope.write("MEASUREMENT:IMMED:SOURCE CH4")
scope.write("MEASUrement:IMMed:TYPe AMP")
for i in range(100):
    scope.write("MEASUrement:IMMed:TYPe AMP")
    scope.query("MEASUREMENT:IMMED:VALUE?")
    scope.write("MEASUrement:IMMed:TYPe FREQ")
    scope.query("MEASUREMENT:IMMED:VALUE?")

scope.get_curve_data(source=4, stop=10000)
MDO3034.curve_to_xy()

