'''
Created on May 7, 2020

@author: Aaron.Scott
'''
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
import numpy as np

def pixel_to_duty(pixels=[0, 1, 0, .7, 1, 0], sub_pixels=10):
    n=len(pixels)
    pix_mat = np.zeros((n,sub_pixels))
    for i in range(n):
        pix_mat[i,0:int(pixels[i]*sub_pixels)]=1
    new_pix_mat = np.reshape(pix_mat, n*sub_pixels)
    return new_pix_mat
pixel_to_duty()

filename = "Ruler.png"
 
img = Image.open(filename)
#img.thumbnail((1000, 500), Image.ANTIALIAS)  # resizes image in-place
data = np.asarray(img)
dims = data.shape
line = data[int(dims[0]/2),:,0]/255
 
plt.plot(line)
 
plt.show()

