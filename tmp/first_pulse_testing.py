import pyvisa
import MDO3034
import AG33511b
import time
import numpy as np
from matplotlib import pyplot as plt

def find_times(x,y, voltage, direction=1):
    if direction == -1:
        x=x[::-1]
        y=y[::-1]
        
    res = next(x for x, val in enumerate(y) if val > voltage)
    return x[res], res

def find_voltages(x,y):
    v_min_avg = np.average(y[0:20])
    n = len(x)
    group = 40
    for i in range(n-group):
        if (y[i]-y[i+group])>.015:
            break
    t0_fall_ind = i+int(group/2)
    t0_fall = x[t0_fall_ind]

    vmax = max(y)
    v100 = y[t0_fall_ind]
    v10 = (v100-v_min_avg)*.1 + v_min_avg
    v90 = (v100-v_min_avg)*.9 + v_min_avg
    return t0_fall, t0_fall_ind, vmax, v100, v10, v90, v_min_avg

h_delay = 5e-3
h_scale = 2e-3


scope = MDO3034.MDO3034(reset=0)
scope.idn()
scope.set_horizontal_scale(h_scale)
scope.set_horizontal_delay_time(h_delay)
record_length = 1000000
scope.write("HORIZONTAL:RECORDLENGTH %i" % record_length)


wfg = AG33511b.AG33511b(reset=0)
wfg.idn()

test_cases = [30e-6, 40e-6, 50e-6, 60e-6, 70e-6, 80e-6, 90e-6, 100e-6]

f= open("results.txt","w")
for t_pix in test_cases:

    #t_pix = 100e-6
    t_gap = 650e-6
    period = t_pix + t_gap
    dc = t_pix/period*100
    n = 1e6
    dt = h_scale*10/n
    t_0 = h_scale*5 - h_delay
    ind_0 = int(t_0/dt)
    n_pix = 10
    ind_pix = int(period/dt)



    #wfg.rst()
    #time.sleep(3)

    wfg.write("SOURCE1:FUNCTION PULSE")
    wfg.write("SOURCE1:FUNCTION:PULSE:PERIOD %.3E" % period)
    wfg.write("FUNCTION:PULSE:DCYCLE %.3E" % dc)
    wfg.write("OUTPUT1:LOAD INF")
    wfg.write("SOURCE1:VOLT 5")
    wfg.write("SOURCE1:VOLT:OFFSET 2.5")
    wfg.set_single_shot(cycles = n_pix)
    wfg.write("OUTPUT1 ON")
    wfg.opc()
    wfg.trigger()
    time.sleep(2)


    scope.save_screen_image(preview=1)
    scope.get_curve_data(start=1,stop=1000000)
    scope.opc()

    ################################################

    t0 = time.time()
    (x,y)=MDO3034.curve_to_xy(preview=1)
    y_min = sum(y[0:100])/100

    fig= plt.figure(figsize=(18,5))
    #plt.style.use('seaborn-darkgrid')
    f.write("%.3E," % t_pix)
    for i in range(n_pix):
        pulse = y[ind_0+i*ind_pix:ind_0+i*ind_pix+ind_pix]-y_min
        p_energy = sum(pulse)
        print(p_energy)
        f.write("%.3E," % p_energy)
    f.write('\n')
    plt.close()

f.close()
