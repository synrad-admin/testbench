import rise_fall
import time


t0 = time.time()
for i in range(100):
    f=open("rise_fall_results.txt", "a")
    rise_time, fall_time = rise_fall.do_rise_fall()
    t1 = time.time()-t0
    message = "%f,%.3E,%.3E" % (t1, rise_time, fall_time)
    print(message)
    f.write(message + "\n")
    f.close()
