import pyvisa
import MDO3034
import AG33511b
import time
import numpy as np
from matplotlib import pyplot as plt

def find_times(x,y, voltage, direction=1):
    if direction == -1:
        x=x[::-1]
        y=y[::-1]
        
    res = next(x for x, val in enumerate(y) if val > voltage)
    return x[res], res

def find_voltages(x,y):
    v_min_avg = np.average(y[0:20])
    n = len(x)
    group = 50
    for i in range(n-group):
        if (y[i]-y[i+group])>.02:
            break
    t0_fall_ind = i+int(group/3)
    t0_fall = x[t0_fall_ind]

    vmax = max(y)
    v100 = y[t0_fall_ind]
    v10 = (v100-v_min_avg)*.1 + v_min_avg
    v90 = (v100-v_min_avg)*.9 + v_min_avg
    return t0_fall, t0_fall_ind, vmax, v100, v10, v90, v_min_avg

def do_rise_fall():
    plt.style.use('default')
    h_scale=100e-6
    h_delay=250e-6
    record_length = 10000
    scope = MDO3034.MDO3034(reset=0)
    scope.idn()
    scope.set_horizontal_scale(h_scale)
    scope.set_horizontal_delay_time(h_delay)
    scope.write("HORIZONTAL:RECORDLENGTH %i" % record_length)

    wfg = AG33511b.AG33511b(reset=0)
    wfg.idn()
    #wfg.rst()
    #time.sleep(3)

    wfg.set_continuous_pwm()
    wfg.write("OUTPUT1 OFF")
    wfg.write("SOURCE1:FUNCTION PULSE")
    wfg.write("SOURCE1:FUNCTION:PULSE:PERIOD 1E-2")
    wfg.write("FUNCTION:PULSE:DCYCLE 5")
    wfg.write("OUTPUT1:LOAD INF")
    wfg.write("SOURCE1:VOLT 5")
    wfg.write("SOURCE1:VOLT:OFFSET 2.5")
    wfg.set_single_shot(cycles = 10)
    wfg.write("OUTPUT1 ON")
    wfg.opc()
    wfg.trigger()
    time.sleep(1)

    scope.save_screen_image(preview=1)
    #scope.get_curve_data(start=400000,stop=407500)
    scope.get_curve_data(start=1,stop=record_length)
    scope.opc()


    ################################################

    t0 = time.time()
    (x,y)=MDO3034.curve_to_xy(preview=0)
    t100_fall, t100_fall_ind, vmax, v100, v10, v90, v_min_avg= find_voltages(x, y)
    t10_rise, t10_rise_ind = find_times(x, y, v10,direction=1)
    t90_rise, t90_rise_ind = find_times(x, y, v90,direction=1)
    t90_fall, t90_fall_ind = find_times(x, y, v90,direction=-1)
    t10_fall, t10_fall_ind = find_times(x, y, v10,direction=-1)

    fig= plt.figure(figsize=(18,5))
    plt.style.use('seaborn-darkgrid')
    plt.plot(x,y,'b')
    plt.plot((x[0],x[-1]), (vmax, vmax), '--k')
    plt.plot((x[0],x[-1]), (v100, v100), '--k')
    plt.plot((x[0],x[-1]), (v90, v90), '--k')
    plt.plot((x[0],x[-1]), (v10, v10), '--k')
    plt.plot((x[0],x[-1]), (v_min_avg, v_min_avg), '--k')

    plt.plot((t10_rise,t10_rise),(min(y), max(y)),'--k')
    plt.plot((t90_rise,t90_rise),(min(y), max(y)),'--k')
    plt.plot((t100_fall,t100_fall),(min(y), max(y)),'--k')
    plt.plot((t90_fall,t90_fall),(min(y), max(y)),'--k')
    plt.plot((t10_fall,t10_fall),(min(y), max(y)),'--k')

    rise_time = (t90_rise - t10_rise)/1e-6
    fall_time = (t10_fall - t90_fall)/1e-6
    pulse_energy = sum(y)*1
    print(pulse_energy)

    t1 = time.time()
    print("Processing Time: %f" % (t1-t0))
    print("Rise Time: %f us" % rise_time)
    print("Fall Time: %f us" % fall_time)
    plt.title("Rise Time: %.1f us, Fall Time: %.1f us" % (rise_time, fall_time))
    plt.show()
    plt.close()
    return rise_time, fall_time
