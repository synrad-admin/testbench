'''
Created on May 5, 2020

@author: Aaron.Scott
'''
import BK9115
import MDO3034
import AG33511b
import time
import csv
import numpy as np
from h5py._hl import datatype

#scope = MDO3034.MDO3034()
#print(scope.idn())
#scope.rst()

if 0:
    ps = BK9115.BK9115()
    ps.set_verbose(1)
    print(ps.idn())
    
    for i in range(48):
        ps.set_voltage(i)
        ps.get_current()
        ps.get_voltage()
        ps.set_output_state(0)
        ps.get_output_state()
        time.sleep(1)
        ps.set_output_state(1)
        ps.get_output_state()
        time.sleep(1)
    
    if __name__ == '__main__':
        pass

if 0:
    scope = MDO3034.MDO3034()
    scope.rst()
    scope.set_verbose(1)
    scope.save_screen_image("tmp/tmp.png", preview=1)
    
if 1:
    
    results = []
    with open('test_data.txt', newline='') as inputfile:
        for row in csv.reader(inputfile):
            results.append(float(row[0]))
            
    results = np.array(results)
    results = results / max(results+1)
    
    print(results)
    wfg = AG33511b.AG33511b()
    wfg.set_verbose(1)
    wfg.rst()
    time.sleep(3)
    print(wfg.idn())
    wfg.write("OUTPUT1 OFF")
#     wfg.write("SOURce1:DATA:VOLatile:CLEar")
#     wfg.err()
#     wfg.write("FORM:BORD NORM")
#     wfg.err()
#     n=10
    rList = np.array([0, 1, 1, 1, 1, 1, 1, 1, 0])*32767
    wfg.load_arb_wf(rList)
    wfg.write("OUTPUT1 ON")
    wfg.set_single_shot()
    wfg.trigger()
    wfg.opc()
    scope = MDO3034.MDO3034()
    scope.idn()
    scope.rst()
    time.sleep(5)
    scope.set_verbose(1)
    scope.save_screen_image("tmp/tmp.png", preview=1)
    

