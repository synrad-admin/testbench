﻿'''

@verbatim

The MIT License (MIT)

Copyright (c) 2021 Bird

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

@endverbatim

@file BirdPyAPI.py

Copyright (c) Bird

@author Afif Bouhenni
'''

import sys
import math
import random
import time
import ctypes
import array
import ctypes.util
import os
import BDS2PyDefs

# Needed for BDS2 DLL
from ctypes import *

class BDS2PyAPI():

    def __init__(self, parent=None):
        self.bds2dll_ = 0
        self.isbds2dll_ = 0
        self.p_bdsdll_handle = c_int()
        self.numMeasRequired = c_int()
        self.numDSAvailable = c_int()
        self.productName = c_char_p()
        self.path = 'C:\\Development\\testbench'
 
    def bds2_loadBDS2DLL(self):
        try:
            self.bds2dll_ = windll.LoadLibrary(self.path + '\\' + "BirdBDS2API.dll")
            print("BDS2PyAPI bds2 dll  loaded ", self.bds2dll_)
            self.isbds2dll_ = 1
        except IOError as e:
            print("BDS2PyAPI cannot load BDS2 dll:", e)

    def bds2_getDLLRevision(self, dllRevision):
        try:
            ret = self.bds2dll_.bds2dll_getDLLRevision(byref(dllRevision))
            return ret
            print("bds2_getDLLRevision  ", self.bds2dll_)
        except IOError as e:
            print("BDS2PyAPI cannot get BDS2 dll Revision:", e)

    def bds2_connect(self, BDS2_index, host_name):
        try:
            print("bds2_connect", host_name)
            p_bdsdll_handle = 0
            if self.isbds2dll_ == 1:
                ret = self.bds2dll_.bds2dll_connectTCPIP(BDS2_index, host_name.encode('utf-8'), 1000)
                print("BDS2PyAPI connect")
                if ret == BDS2PyDefs.BDS2_errors.e_bds_bcerrSuccess:
                    self.isbds2dll_ = 2;
                self.isbds2dll_ = 2;
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot connect to BDS2:", e)


    def bds2_disconnect(self, BDS2_index):
        try:
            print("BDS2PyAPI bds2_disconnect  ", self.isbds2dll_)
            if self.isbds2dll_ > 0:
                ret = self.bds2dll_.bds2dll_disconnect(BDS2_index)
                self.isbds2dll_ = 1;
                self.p_bdsdll_handle = 0
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot connect to BDS2:", e)

    def bds2_getBDS2Revision(self, BDS2_index, BDS2Revision):
        try:
            if self.isbds2dll_ > 1:
                ret = self.bds2dll_.bds2dll_getBDS2Revision(BDS2_index, byref(BDS2Revision))
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot get BDS2 revision info:", e)

    def bds2_getSystemInfo(self, BDS2_index, systemInfo):
        try:
            if self.isbds2dll_ > 1:
                ret = self.bds2dll_.bds2dll_getSystemInfo(BDS2_index, byref(systemInfo))
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot get BDS2 system info:", e)

    def bds2_initAttenuation(self, BDS2_index, BDS2Attn):
        try:
            if self.isbds2dll_ > 1:
                print("BDS2PyAPI init atten ")
                ret = self.bds2dll_.bds2dll_initAttenuation(BDS2_index, byref(BDS2Attn))
                print("BDS2PyAPI init atten ", ret, BDS2Attn.isAGCOn, BDS2Attn.attnVdB, BDS2Attn.attnIdB)
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot initialize the attenuation:", e)

    def bds2_setAttenuation(self, BDS2_index, BDS2Attn):
        try:
            if self.isbds2dll_ > 1:
                print("BDS2PyAPI setting atten ", BDS2Attn.isAGCOn, BDS2Attn.attnVdB, BDS2Attn.attnIdB)
                ret = self.bds2dll_.bds2dll_setAttenuation(BDS2_index, byref(BDS2Attn))
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot set the attenuation:", e)
   
    def bds2_getAttenuation(self, BDS2_index, BDS2Attn):
        try:
            if self.isbds2dll_ > 1:
                ret = self.bds2dll_.bds2dll_getAttenuation(BDS2_index, byref(BDS2Attn))
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot get the attenuation:", e)
      
    def bds2_setAverageConfig(self, BDS2_index, BDS2AvgConfig):
        try:
            if self.isbds2dll_ > 1:
                ret = self.bds2dll_.bds2dll_setAvgConfig(BDS2_index, byref(BDS2AvgConfig))
                return ret
        except IOError as e:
            print("BDS2PyAPI cannot set the average config:", e)  

    def bds2_initStdConfig(self, BDS2_index, BDS2Config, fund, states):
       try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_initStdConfig(BDS2_index, byref(BDS2Config), byref(fund), byref(states))
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot initialize the std tracking  config:", e)

    def bds2_setStdConfig(self, BDS2_index, BDS2Config, fund, states):
       try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_setStdConfig(BDS2_index, byref(BDS2Config), byref(fund), byref(states))
               print("BDS2PyAPI set config numreq ", BDS2Config.numMeasReq , BDS2Config.numFunds, BDS2Config.displayUpdateRate)
               self.isbds2dll_ = 3;
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot set the std tracking  config:", e)
       
    def bds2_setStartScan(self, BDS2_index):
       try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_startScan(BDS2_index)
               self.isbds2dll_ = 3;
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot start scan:", e)

    def bds2_stopScan(self, BDS2_index):
       try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_stopScan(BDS2_index)
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot stop scan:", e)

    def bds2_getStdConfig(self, BDS2_index, BDS2Config, fund, states):
       try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_getStdConfig(BDS2_index, byref(BDS2Config), byref(fund), byref(states))
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot get the std tracking  config:", e)

    def bds2_getStdNumMeasurements(self, BDS2_index, numMeasurements):
       try:
            if self.isbds2dll_ > 1:
               self.numMeasRequired = c_int()
               ret = self.bds2dll_.bds2dll_getStdNumMeasurements(BDS2_index, byref(self.numMeasRequired))
               numMeasurements = self.numMeasRequired
               return ret
       except IOError as e:
            print("BDS2PyAPI cannot get std tracking numMeasurements:", e)

    def bds2_getStdDataSet(self, BDS2_index, BDS2Header, BDS2Meas):
        try:
            if self.isbds2dll_ > 1:
               ret = self.bds2dll_.bds2dll_getStdSingleDataset(BDS2_index, byref(BDS2Header), byref(BDS2Meas))
               return ret
        except IOError as e:
            print("BDS2PyAPI cannot get std tracking datasets:", e)

    def bds2_getLastError(self, BDS2Error):
        try:
            ret = self.bds2dll_.bds2dll_getLastError(byref(BDS2Error))

        except IOError as e:
            print("BDS2PyAPI cannot get std tracking datasets:", e)
        return ret

if __name__ == "__main__":
    myapp = BDS2PyAPI()
