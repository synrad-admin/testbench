#Author: Brendan Roozen- Research & Development Team: Synrad
#Date: 16 July 2021
#File description: Short-test script that allows the user to turn the Power Supply-BK9115 on and off.
#File allows user to set both the current and voltage level. 

#import the subclass BK9115 from instruments folder
from instruments import BK9115

#BK9115 as an object to call the methods within the class
ps = BK9115.BK9115()

#Device information
ps.idn()

#Turn power supply off
ps.set_output_state("OFF")

#set the paramters
ps.set_voltage(48)
ps.set_current(20)

#Turn device one
ps.set_output_state("ON")

#Get the current voltage values
current = ps.get_current()
voltage = ps.get_voltage()

#Record current and voltage into file
f = open("test.txt", "w")
f.write("Current: \t%f\n" % current)
f.write("Current: \t%f\n" % voltage) 
