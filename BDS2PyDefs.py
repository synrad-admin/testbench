﻿'''

@verbatim

The MIT License (MIT)

Copyright (c) 2021 Bird

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

@endverbatim

@file BirdPyAPI.py

Copyright (c) Bird

@author Afif Bouhenni
'''
import sys
import time
import ctypes
import array
import ctypes.util
import os



from ctypes import *

FIRST_USER_ERROR = 0x2000000
class BDS2_errors():
    e_bds_BIRD_COPYRIGHT =FIRST_USER_ERROR
    e_bds_bcerrUnknown = FIRST_USER_ERROR+1		                 # Unknown error - host or client did not set an error code.          0x0001
    e_bds_bcerrSuccess = FIRST_USER_ERROR+2						 # No error - successful operation.
    e_bds_bcerrNotConnected = FIRST_USER_ERROR+3				 # Client not connected to host.
    e_bds_bcerrConnectionRejected = FIRST_USER_ERROR+4			 # Client connection rejected by host.
    e_bds_bcerrUnknownMsg = FIRST_USER_ERROR+5					 # Unknown message ID received.
    e_bds_bcerrUnknownConn = FIRST_USER_ERROR+6				     # Unknown connection instance ID received.
    e_bds_bcerrUnknownService = FIRST_USER_ERROR+7				 # Unknown service code received.
    e_bds_bcerrUnknownClass = FIRST_USER_ERROR+8				 # Unknown class ID received.                                         0x0008
    e_bds_bcerrUnknownInst = FIRST_USER_ERROR+9			         # Unknown object instance number received.
    e_bds_bcerrUnknownAttrib = FIRST_USER_ERROR+10				 # Unknown class attribute or method ID.
    e_bds_bcerrInvalidLength = FIRST_USER_ERROR+11				 # Invalid packet length.
    e_bds_bcerrParse = FIRST_USER_ERROR+12						 # Invalid packet data received - unable to parse.
    e_bds_bcerrCorruptedData = FIRST_USER_ERROR+13				 # Corrupted data section (invalid data integrity value).
    e_bds_bcerrAccess = FIRST_USER_ERROR+14					     # Client does not have proper access rights.
    e_bds_bcerrCmdNotSent = FIRST_USER_ERROR+15					 # Received a response for a command that was not sent.
    e_bds_bcerrInvalidParam = FIRST_USER_ERROR+16			     # Invalid parameter value (out-of-range).                            0x0010
    e_bds_bcerrInsufficientBuffer = FIRST_USER_ERROR+17			 # Not enough room to store request data.
    e_bds_bcerrUnsupportedProtocol = FIRST_USER_ERROR+18		 # Client protocol is not supported by host.
    e_bds_bcerrFatal = FIRST_USER_ERROR+19						 # Fatal error occurred internal to the host or client.
    e_bds_bcerrInsufficientMemory = FIRST_USER_ERROR+20			 # Not enough memory to complete allocation command.
    e_bds_bcerrServiceNotImplemented = FIRST_USER_ERROR+21		 # The service code is known but not implemented.
    e_bds_bcerrServiceNotAllowed = FIRST_USER_ERROR+22			 # The service code is known but not allowed.
    e_bds_bcerrUnidentifiedClient = FIRST_USER_ERROR+23			 # The client did not identify itself to the host.
    e_bds_bcerrDataNotAvailable = FIRST_USER_ERROR+24			 # No data available for data poll request.                           0x0018
    e_bds_bcerrInterruptOutOfSynch = FIRST_USER_ERROR+25		 # Interrupt out-of-sync.
    e_bds_bcerrTimeout = FIRST_USER_ERROR+26					 # Operation time-out.
    e_bds_bcerrCommunicationFailure = FIRST_USER_ERROR+27		 # Low-level communications failure (i.e. socket read/write error).
    e_bds_bcerrIO = FIRST_USER_ERROR+33	                         # I/O file read/write error.                                         0x0021
    e_bds_bcerrFeatureUnavailable = FIRST_USER_ERROR+34			 # Unsupported feature for a specific model.

    e_bds_berrApiStart = FIRST_USER_ERROR+40,                    # Not an error.                                                      0x0028				
    e_bds_bcerrInvalidArgument = FIRST_USER_ERROR+41             # Invalid function call argument.
    e_bds_bcerrInvalidHandle = FIRST_USER_ERROR+42				 # Invalid DLL handle.                            

    e_bds_bcerrInvalidAGC = FIRST_USER_ERROR+43					 # Invalid AGC setting.
    e_bds_bcerrInvalidAttenIdB = FIRST_USER_ERROR+44			 # Invalid current attenuation.
    e_bds_bcerrInvalidAttenVdB = FIRST_USER_ERROR+45			 # Invalid voltage attenuation.

    e_bds_bcerrInvalidAttenResetDelay = FIRST_USER_ERROR+46		 # Invalid attenuation reset delay.

    e_bds_bcerrInvalidNumFunds = FIRST_USER_ERROR+47			 # Invalid number of fundamentals.

    e_bds_bcerrFund1FreqOutofRange = FIRST_USER_ERROR+48		 # Fundamental 1 frequency out of range (300000 to 253000000).        0x0030
    e_bds_bcerrFund2FreqOutofRange = FIRST_USER_ERROR+49		 # Fundamental 2 frequency out of range (300000 to 253000000).
    e_bds_bcerrFund3FreqOutofRange = FIRST_USER_ERROR+50		 # Fundamental 3 frequency out of range (300000 to 253000000).

    e_bds_bcerrInvalidFund1Harm1 = FIRST_USER_ERROR+51			 # Fundamental 1 invalid harmonic 1
    e_bds_bcerrInvalidFund1Harm2 = FIRST_USER_ERROR+52			 # Fundamental 1 invalid harmonic 2
    e_bds_bcerrInvalidFund1Harm3 = FIRST_USER_ERROR+53			 # Fundamental 1 invalid harmonic 3
    e_bds_bcerrInvalidFund1Harm4 = FIRST_USER_ERROR+54			 # Fundamental 1 invalid harmonic 4

    e_bds_bcerrInvalidFund2Harm1 = FIRST_USER_ERROR+55			 # Fundamental 2 invalid harmonic 1
    e_bds_bcerrInvalidFund2Harm2 = FIRST_USER_ERROR+56			 # Fundamental 2 invalid harmonic 2                                   0x0038
    e_bds_bcerrInvalidFund2Harm3 = FIRST_USER_ERROR+57			 # Fundamental 2 invalid harmonic 3
    e_bds_bcerrInvalidFund2Harm4 = FIRST_USER_ERROR+58			 # Fundamental 2 invalid harmonic 4

    e_bds_bcerrInvalidFund3Harm1 = FIRST_USER_ERROR+59			 # Fundamental 3 invalid harmonic 1
    e_bds_bcerrInvalidFund3Harm2 = FIRST_USER_ERROR+60			 # Fundamental 3 invalid harmonic 2
    e_bds_bcerrInvalidFund3Harm3 = FIRST_USER_ERROR+61			 # Fundamental 3 invalid harmonic 3
    e_bds_bcerrInvalidFund3Harm4 = FIRST_USER_ERROR+62			 # Fundamental 3 invalid harmonic 4

    e_bds_bcerrFund1Harm1FreqOutofRange = FIRST_USER_ERROR+63	 # Fundamental 1 harmonic 1 frequency out of range
    e_bds_bcerrFund1Harm2FreqOutofRange = FIRST_USER_ERROR+64	 # Fundamental 1 harmonic 2 frequency out of range                    0x0040
    e_bds_bcerrFund1Harm3FreqOutofRange = FIRST_USER_ERROR+65	 # Fundamental 1 harmonic 3 frequency out of range
    e_bds_bcerrFund1Harm4FreqOutofRange = FIRST_USER_ERROR+66	 # Fundamental 1 harmonic 4 frequency out of range

    e_bds_bcerrFund2Harm1FreqOutofRange = FIRST_USER_ERROR+67	 # Fundamental 2 harmonic 1 frequency out of range
    e_bds_bcerrFund2Harm2FreqOutofRange = FIRST_USER_ERROR+68	 # Fundamental 2 harmonic 2 frequency out of range
    e_bds_bcerrFund2Harm3FreqOutofRange = FIRST_USER_ERROR+69	 # Fundamental 2 harmonic 3 frequency out of range
    e_bds_bcerrFund2Harm4FreqOutofRange = FIRST_USER_ERROR+70	 # Fundamental 2 harmonic 4 frequency out of range

    e_bds_bcerrFund3Harm1FreqOutofRange = FIRST_USER_ERROR+71	 # Fundamental 3 harmonic 1 frequency out of range
    e_bds_bcerrFund3Harm2FreqOutofRange = FIRST_USER_ERROR+72	 # Fundamental 3 harmonic 2 frequency out of range                    0x0048
    e_bds_bcerrFund3Harm3FreqOutofRange = FIRST_USER_ERROR+73	 # Fundamental 3 harmonic 3 frequency out of range
    e_bds_bcerrFund3Harm4FreqOutofRange = FIRST_USER_ERROR+74	 # Fundamental 3 harmonic 4 frequency out of range

    e_bds_bcerrInvalidFund1IMDFund2 = FIRST_USER_ERROR+75		 # Fundamental 1 invalid secondary fundamental for IMD
    e_bds_bcerrInvalidFund2IMDFund2 = FIRST_USER_ERROR+76		 # Fundamental 2 invalid secondary fundamental for IMD
    e_bds_bcerrInvalidFund3IMDFund2 = FIRST_USER_ERROR+77		 # Fundamental 3 invalid secondary fundamental for IMD

    e_bds_bcerrInvalidFund1IMDSel = FIRST_USER_ERROR+78			 # Fundamental 1 invalid IMD selection
    e_bds_bcerrInvalidFund2IMDSel = FIRST_USER_ERROR+79			 # Fundamental 2 invalid IMD selection
    e_bds_bcerrInvalidFund3IMDSel = FIRST_USER_ERROR+80			 # Fundamental 3 invalid IMD selection                                0x0050

    e_bds_bcerrFund1IMDFreqOutofRange = FIRST_USER_ERROR+81		 # Fundamental 1 IMD frequency out of range
    e_bds_bcerrFund2IMDFreqOutofRange = FIRST_USER_ERROR+82		 # Fundamental 2 IMD frequency out of range
    e_bds_bcerrFund3IMDFreqOutofRange = FIRST_USER_ERROR+83		 # Fundamental 3 IMD frequency out of range

    e_bds_bcerrInvalidFund1DFilter = FIRST_USER_ERROR+84		 # Fundamental 1 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)
    e_bds_bcerrInvalidFund2DFilter = FIRST_USER_ERROR+85		 # Fundamental 2 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)
    e_bds_bcerrInvalidFund3DFilter = FIRST_USER_ERROR+86		 # Fundamental 3 invalid detection filter (1MHz, 100kHz, 10kHz, or 1kHz)

    e_bds_bcerrInvalidFund1MeasMode = FIRST_USER_ERROR+87		 # Fundamental 1 invalid measurement mode (CW, Pulse, Multi-Level)
    e_bds_bcerrInvalidFund2MeasMode = FIRST_USER_ERROR+88		 # Fundamental 2 invalid measurement mode (CW, Pulse, Multi-Level)    0x0058
    e_bds_bcerrInvalidFund3MeasMode = FIRST_USER_ERROR+89		 # Fundamental 3 invalid measurement mode (CW, Pulse, Multi-Level)

    e_bds_bcerrInvalidFund1TrkMode = FIRST_USER_ERROR+90		 # Fundamental 1 invalid tracking mode (Fixed or Tracking)
    e_bds_bcerrInvalidFund2TrkMode = FIRST_USER_ERROR+91		 # Fundamental 2 invalid tracking mode (Fixed or Tracking)
    e_bds_bcerrInvalidFund3TrkMode = FIRST_USER_ERROR+92		 # Fundamental 3 invalid tracking mode (Fixed or Tracking)

    e_bds_bcerrInvalidFund1viTrk = FIRST_USER_ERROR+93			 # Fundamental 1 invalid signal tracking (V, I, or Auto)
    e_bds_bcerrInvalidFund2viTrk = FIRST_USER_ERROR+94			 # Fundamental 2 invalid signal tracking (V, I, or Auto)
    e_bds_bcerrInvalidFund3viTrk = FIRST_USER_ERROR+95			 # Fundamental 3 invalid signal tracking (V, I, or Auto)

    e_bds_bcerrFund1iNoiseLvlOutofRange = FIRST_USER_ERROR+96	 # Fundamental 1 current noise level out of range                     0x0060
    e_bds_bcerrFund2iNoiseLvlOutofRange = FIRST_USER_ERROR+97	 # Fundamental 2 current noise level out of range
    e_bds_bcerrFund3iNoiseLvlOutofRange = FIRST_USER_ERROR+98	 # Fundamental 3 current noise level out of range

    e_bds_bcerrFund1vNoiseLvlOutofRange = FIRST_USER_ERROR+99	 # Fundamental 1 voltage noise level out of range
    e_bds_bcerrFund2vNoiseLvlOutofRange = FIRST_USER_ERROR+100	 # Fundamental 2 voltage noise level out of range
    e_bds_bcerrFund3vNoiseLvlOutofRange = FIRST_USER_ERROR+101	 # Fundamental 3 voltage noise level out of range

    e_bds_bcerrFund1State1FreqOutofRange = FIRST_USER_ERROR+102	 # ML Fundamental 1 State 1 seed frequency out of range
    e_bds_bcerrFund1State2FreqOutofRange = FIRST_USER_ERROR+103	 # ML Fundamental 1 State 2 seed frequency out of range
    e_bds_bcerrFund1State3FreqOutofRange = FIRST_USER_ERROR+104	 # ML Fundamental 1 State 3 seed frequency out of range               0x0068
    e_bds_bcerrFund1State4FreqOutofRange = FIRST_USER_ERROR+105	 # ML Fundamental 1 State 4 seed frequency out of range

    e_bds_bcerrFund2State1FreqOutofRange = FIRST_USER_ERROR+106	 # ML Fundamental 2 State 1 seed frequency out of range
    e_bds_bcerrFund2State2FreqOutofRange = FIRST_USER_ERROR+107	 # ML Fundamental 2 State 2 seed frequency out of range
    e_bds_bcerrFund2State3FreqOutofRange = FIRST_USER_ERROR+108	 # ML Fundamental 2 State 3 seed frequency out of range
    e_bds_bcerrFund2State4FreqOutofRange = FIRST_USER_ERROR+109	 # ML Fundamental 2 State 4 seed frequency out of range

    e_bds_bcerrFund3State1FreqOutofRange = FIRST_USER_ERROR+110	 # ML Fundamental 3 State 1 seed frequency out of range
    e_bds_bcerrFund3State2FreqOutofRange = FIRST_USER_ERROR+111	 # ML Fundamental 3 State 2 seed frequency out of range
    e_bds_bcerrFund3State3FreqOutofRange = FIRST_USER_ERROR+112	 # ML Fundamental 3 State 3 seed frequency out of range               0x0070
    e_bds_bcerrFund3State4FreqOutofRange = FIRST_USER_ERROR+113	 # ML Fundamental 3 State 4 seed frequency out of range

    e_bds_bcerrInvalidNumTrkStates = FIRST_USER_ERROR+114		 # ML invalid number of states.

    e_bds_bcerrTrkState1DelayTooSmall = FIRST_USER_ERROR+115	 # ML State 1 delay too small for the selected detection filter
    e_bds_bcerrTrkState2DelayTooSmall = FIRST_USER_ERROR+116	 # ML State 2 delay too small for the selected detection filter
    e_bds_bcerrTrkState3DelayTooSmall = FIRST_USER_ERROR+117	 # ML State 3 delay too small for the selected detection filter
    e_bds_bcerrTrkState4DelayTooSmall = FIRST_USER_ERROR+118	 # ML State 4 delay too small for the selected detection filter

    e_bds_bcerrTrkState1DelayTooBig = FIRST_USER_ERROR+119		 # ML State 1 delay too big for the selected detection filter
    e_bds_bcerrTrkState2DelayTooBig = FIRST_USER_ERROR+120		 # ML State 2 delay too big for the selected detection filter         0x0078
    e_bds_bcerrTrkState3DelayTooBig = FIRST_USER_ERROR+121		 # ML State 3 delay too big for the selected detection filter
    e_bds_bcerrTrkState4DelayTooBig = FIRST_USER_ERROR+122		 # ML State 3 delay too big for the selected detection filter

    e_bds_bcerrTrkState1DurationTooShort = FIRST_USER_ERROR+123	 # ML State 1 duration too short for the selected detection filter
    e_bds_bcerrTrkState2DurationTooShort = FIRST_USER_ERROR+124	 # ML State 2 duration too short for the selected detection filter
    e_bds_bcerrTrkState3DurationTooShort = FIRST_USER_ERROR+125	 # ML State 3 duration too short for the selected detection filter
    e_bds_bcerrTrkState4DurationTooShort = FIRST_USER_ERROR+126	 # ML State 4 duration too short for the selected detection filter

    e_bds_bcerrTrkState1DurationTooLong = FIRST_USER_ERROR+127	 # ML State 1 duration too long for the selected detection filter
    e_bds_bcerrTrkState2DurationTooLong = FIRST_USER_ERROR+128	 # ML State 2 duration too long for the selected detection filter     0x0080
    e_bds_bcerrTrkState3DurationTooLong = FIRST_USER_ERROR+129	 # ML State 3 duration too long for the selected detection filter
    e_bds_bcerrTrkState4DurationTooLong = FIRST_USER_ERROR+130	 # ML State 4 duration too long for the selected detection filter

    e_bds_bcerrPulsePeriodTooShort = FIRST_USER_ERROR+131		 # ML Pulse period too short
    e_bds_bcerrPulsePeriodOutOfRange = FIRST_USER_ERROR+132		 # ML Pulse period out of range
    e_bds_bcerrInvalidComponentId = FIRST_USER_ERROR+133		 # Invalid component ID
    e_bds_bcerrInvalidNumFreqComponents = FIRST_USER_ERROR+134   # Invalid Number of frequency components

    e_bds_bcerrPulseTriggerDelayOutofRange = FIRST_USER_ERROR+135# Pulse rate delay out of range.
    e_bds_bcerrDataUpdateRateOutofRange = FIRST_USER_ERROR+136	 # Data Update Rate out of range.                                     0x0088
    e_bds_bcerrStdConfigNotInitialized = FIRST_USER_ERROR+137    # Std configuration not initialized
    e_bds_bcerrInvalidAverageWindowSize = FIRST_USER_ERROR+138	 # Invalid Average window size.
    e_bds_bcerrInvalidNumMultiStatesComponents = FIRST_USER_ERROR+139 # Invalid Number of Multi-states frequency components

    e_bds_bcerrLast = FIRST_USER_ERROR + 140						 # <-- Not an error - marks the last in the enum list!

	

BDS2_MAX_CLIENTS = 4		              # maximum number of BDS2 receivers
										  # that the API can simultaneously connect to.

BDS2_MAX_FUNDAMENTALS = 3                 # maximum number of fundamentals
BDS2_MAX_HARMONICS = 5		              # fundamental(1st harmonic) + 4 
BDS2_MAX_INTERMODS = 6

BDS2_MAX_STATES = 4			              # maximum number of states
BDS2_FRAME_MAX_LEN = 1000000              # in usecs (1 second)
BDS2_FRAME_MIN_LEN = 20                   # in usecs (20 microSeconds)
BDS2_STATE_DELAY_MAX = 500000             # in usecs (0.5 seconds)
BDS2_STATE_DELAY_MIN = 0                  # in usecs (0 microSeconds) 
BDS2_STATE_DUR_MAX = 500000               # in usecs (0.5 seconds)
BDS2_STATE_DUR_MIN = 5                    # in usecs (5 microSeconds) 

BDS2_MAX_V_NOISE_LVL = 5                  #5 volts
BDS2_MIN_V_NOISE_LVL = 0                  #0 volts
BDS2_DEFAULT_V_NOISE_LVL = 2.5            #2.5 volts

BDS2_MAX_I_NOISE_LVL = .1                 #100 milliamps
BDS2_MIN_I_NOISE_LVL = 0                  #0 amps
BDS2_DEFAULT_I_NOISE_LVL = .03            #30 milliamps

BDS2_PULSE_SYNC_DELAY_MAX = 500000		  #in usecs (0.5 seconds)
BDS2_PULSE_SYNC_DELAY_MIN = 0			  #in usecs (0 microSeconds)

BDS2_STD_MAX_DATA_UPDATE_RATE = 600000	  #in milliSeconds (10 minutes)
BDS2_STD_MIN_DATA_UPDATE_RATE = 2		  #in milliSeconds (2 milliSeconds)
BDS2_STD_MIN_RS232_DATA_UPDATE_RATE = 20  #in milliSeconds ()20 milliSeconds)

BDS2_MAX_NUM_FREQ_COMPONENTS = 12         # 12 frequency components
BDS2_MAX_NUM_DATA_SETS = 48               # 12 frequency components * BDS2_MAX_STATES
BDS2_MAX_NUM_MULTI_STATE_MEAS = 20        # Maximum number of multi-states measurements.

BDS2_MAX_ERROR_MES_SIZE = 256

'''
	The lowest possible frequency.
'''
BDS2_STD_MIN_FREQ = 308000

'''
	The highest possible frequency.
'''
BDS2_STD_MAX_FREQ = 252000000

'''
	The maximum window size for "simple" average mode.
'''
BDS2_AVG_MAX_WIN_SIZE_SIMPLE = 16

BDS2_MAX_DATA_UPDATE_RATE = 100 # 100 milliSeconds

'''
	Gregorian date structure.
	Format:
	date: YYYYMMDD
		YYYY = 4-digit year (0-9999), 
		MM   = 2-digit month (1-12),
		DD   = 2-digit day (1-31)

	To date: date = (year * 10000) + (month * 100) + day
	From date: 
		year = date/10000
		month = (date - (year * 10000))/100
		day = date - (year * 10000) - (month * 100)
'''

BDS2_Date = 20210301 # Packed date (YYYYMMDD).

#
#
#
class BDS2_eautoAGC():
	e_bds2AGC_OFF=0	    # AGC OFF / manual attenuation setting
	e_bds2AGC_ON=1		# AGC ON / auto
#
#
#
class BDS2_eManualAttunation():
	e_bds2Attn0dB=0		    # AGC manual attenuation 0  dB.
	e_bds2Attn6dB=6		    # AGC manual attenuation 6  dB.
	e_bds2Attn12dB=12		# AGC manual attenuation 12 dB.
	e_bds2Attn18dB=18		# AGC manual attenuation 18 dB.
	e_bds2Attn24dB=24		# AGC manual attenuation 24 dB.
	e_bds2Attn30dB=30		# AGC manual attenuation 30 dB.
#
#
#
class BDS2_eResetDelay():
	e_bds2ResetDelay1ms=1		# AGC attenuation reset delay  1 milliSecond.
	e_bds2ResetDelay2ms=2		# AGC attenuation reset delay  2 milliSeconds.
	e_bds2ResetDelay4ms=4		# AGC attenuation reset delay  4 milliSeconds.
	e_bds2ResetDelay10ms=10	    # AGC attenuation reset delay  10 milliSeconds.
	e_bds2ResetDelay20ms=20	    # AGC attenuation reset delay  20 milliSeconds.
	e_bds2ResetDelay40ms=40	    # AGC attenuation reset delay  40 milliSeconds.
	e_bds2ResetDelay100ms=100	# AGC attenuation reset delay  100 milliSeconds.


#
#	BDS measurement mode.
#
class BDS2_eMeasMode():
	e_bds2_CWMode=0		    # Std CW Tracking Mode. 
	e_bds2_PulsedMode=1		# Std Pulsed Tracking Mode. 
	e_bds2_MultiStateMode=2	# Std Multi-State Tracking Mode. 


#
#	BDS tracking mode.
#
class BDS2_eTrackingMode():
	e_bds2_FixedFreq=0 	    # Std Fixed Frequency Mode. 
	e_bds2_TrackingFreq=1 	# Std Frequency Tracking Mode.


#
#	BDS tracking mode.
#
class BDS2_eVITracking():
	e_bds2_AutoTracking=0 	# Std Auto Tracking Mode. 
	e_bds2_VTracking=1 		# Std Voltage Tracking Mode. 
	e_bds2_ITracking=2 		# Std Current Tracking Mode. 


#
#	BDS detection filters.
#
class BDS2_eDetFilter():
	e_bds2_1MHz=0        #  1 MHz detection filter.
	e_bds2_100kHz=1      #  100 kHz detection filter.   
	e_bds2_10kHz=2  	 #  10 kHz detection filter. 
	e_bds2_1kHz =3 	     #  1 kHz detection filter.

#
#	BDS detection filter delays.
#
class BDS2_eDetFilterDelay():
	e_bds2_1MHzDelay_us=2     #    2 microSeconds.
	e_bds2_100kHzDelay_us=16  #   16 microSeconds.
	e_bds2_10kHzDelay_us=131  #  131 microSeconds.
	e_bds2_1kHzDelay_us=1048  # 1048 microSeconds.
#
#
#
class BDS2_eHarmonicSel():
	e_bds2_HarmSelOFF=0	     # harmonic frequency not selected.
	e_bds2_HarmSelON=1		 #  harmonic frequency selected.


#
#	Intermodulation product selections.
#	Intermodulation number (value).
#	Intermodulation product +/- nF2.
#	-3 = F1 - 3F2
#	-2 = F1 - 2F2
#	-1 = F1 - F2
#	0 = No intermodulation.
#	1 = F1 + F2
#	2 = F1 + 2F2
#	3 = F1 + 3F2
#
class BDS2_eIntermodSel():
	e_bds2_IMNone=0  	     # no intermodulation products.
	e_bds2_IM1Plus1Minus=1   # 1 "plus" and 1 "minus" product.
	e_bds2_IM2Plus2Minus=2   # 2 "plus" and 2 "minus" products.
	e_bds2_IM3Plus3Minus=3   # 3 "plus" and 2 "minus" products.

#
#
#
class BDS2_eintermodFund2():
	e_bds2_IMFund2_None=0   # intermodulation no second fundamental.
	e_bds2_IMFund2_1=1		# intermodulation fundamental 1 as second fundamental.
	e_bds2_IMFund2_2=2		# intermodulation fundamental 2 as second fundamental.
	e_bds2_IMFund2_3=3		# intermodulation fundamental 3 as second fundamental.

#
#	BDS2 Component ID.
#
class BDS2_eComponentId():
	e_bds2_probe=0      # probe component ID. 
	e_bds2_cable=1 	    # cable component ID. 
	e_bds2_receiver=2   # receiver component ID. 

#
#
#
class BDS2_eStatus():
	e_bds2StatusIdle=0				# Idle
	e_bds2StatusAGCOff=1		    # AGC ON/OFF status
	e_bds2StatusVOver=2			    # Voltage overflow
	e_bds2StatusIOver=3			    # Current overflow
	e_bds2StatusProbeDisconnect=4	# Probe disconnected
	e_bds2StatusNotCal=5			# Calibration off
	e_bds2StatusArcDetected=6		# Arc detected


#
#
#
class bdsErrorData(Structure):
    _fields_ = [("ErrorCode", c_int), ("ErrorMessage", type(create_string_buffer(BDS2_MAX_ERROR_MES_SIZE)))]

#
#
#    
class DLLVersionStruct(Structure):
    _fields_ = [("dllName",type(create_string_buffer(256))), ("major",c_short), ("minor",c_short), ("date",c_uint)]
#
#
#
class BDS2VersionStruct(Structure):
    _fields_ = [("productName",type(create_string_buffer(256))), ("major",c_short), ("minor",c_short), ("date",c_uint)]

#
#
#
class SystemInfoStruct(Structure):
    _fields_ = [("componentId",c_int), ("componentName",type(create_string_buffer(128))), ("model",type(create_string_buffer(128))), ("serialNumber",type(create_string_buffer(128))),]

# Packed date (YYYYMMDD).
# Packed time (HHMMSS).
# Packed high resolution time (mmmuuu).

class BDS2TimestampStruct(Structure):
    _fields_ = [("date",c_uint),("time",c_uint),("hres",c_uint)]

# 4-digit year (0-9999).
# 2-digit month (1-12).
# 2-digit day (1-31).
# 2-digit hour (0-23).
# 2-digit minutes (0-59).
# 2-digit seconds (0-59).
# 3-digit milliSeconds (0-999).
# 3-digit microSeconds (0-999).
class BDS2_DateAndTime(Structure):
    _fields_ = [("year",c_short),("month",c_short),("day",c_short),
	("hours",c_short),("minutes",c_short),("seconds",c_short),
	("millis",c_short),("usec",c_short)]

class BDS2AttnStruct(Structure):
    _fields_ = [("isAGCOn",c_ubyte),("attnVdB",c_ubyte),("attnIdB",c_ubyte),("resetDelayms",c_ubyte)]

class BDS2AvgStruct(Structure):
    _fields_ = [("isAverageEnabled",c_ubyte),("averageWindowSize",c_uint)]

class BDS2StdCfgStruct(Structure):
    _fields_ = [("numFunds",c_uint), ("pulseTrigerDelay",c_uint), ("displayUpdateRate",c_uint), ("numStates",c_uint), ("pulsePeriod",c_uint), ("numMeasReq",c_uint)]

class BDS2StdFundStruct(Structure):
    _fields_ = [("freqHz",c_uint), ("harmSel",c_uint*(BDS2_MAX_HARMONICS)), ("intermodFund2",c_uint), ("intermodSel",c_uint), ("detFilter",c_uint), 
	("vNoiseLevel",c_float), ("iNoiseLevel",c_float), ("viTracking",c_uint), ("measMode",c_uint), ("trackingMode",c_uint), ("stateFreq",c_uint*BDS2_MAX_STATES)]

class BDS2StdStateStruct(Structure):
    _fields_ = [("trkStateDelay",c_uint), ("trkStateDuration",c_uint), ("trkStateNumberOfFrames",c_uint)]

class BDS2StdMeasHeaderStruct(Structure):
    _fields_ = [("status",c_uint), ("numMeas",c_ubyte), ("timestamp",BDS2TimestampStruct), ("numAvg",c_ubyte*BDS2_MAX_FUNDAMENTALS)]

class BDS2StdMeasDataStruct(Structure):
    _fields_ = [("fnum",c_ubyte), ("hnum",c_ubyte), ("inum",c_ubyte), ("snum",c_ubyte), ("attnV",c_ubyte), ("attnI",c_ubyte),
    ("freq",c_uint), ("voltage",c_float), ("current",c_float), ("phase",c_float), ("Zmag",c_double), ("R",c_double), ("X",c_double),
    ("delPower",c_double),("fwdPower",c_double), ("rflPower",c_double)]

