import AG33511b
import MDO3034
import time

# Connect to instruments #######################################################
awg = AG33511b.AG33511b(reset=0)
oscope = MDO3034.MDO3034(reset=0)

# Configure oscilloscope #######################################################

# Configure timebase
oscope.set_horizontal_scale(8e-6)
oscope.set_horizontal_delay_time(32e-6)

# Configure fast detector channel
oscope.display_channel(1, "ON")             # Fast detector in channel 1
oscope.set_ch_termination(1, "FIFTY")
oscope.set_ch_position(1, -3)
oscope.set_ch_scale(1, .05)
oscope.set_ch_bandwidth(1,20e6)

# Configure trigger channel 
oscope.display_channel(4, "ON")             # channel 4 is trigger signal
oscope.set_ch_scale(4,5)                    # set channel 4 to 2 V/div
oscope.set_normal_edge_trigger(4,2)         # set channel 4 to normal, rising
                                            # edge trigger, trigger at 2V
oscope.set_single_shot()

# Configure function generator #################################################
awg.output_load("1000")                      # set output load to high impedance

# Execute test #################################################################
awg.set_continuous_pwm()

# Process data #################################################################
oscope.save_screen_image(preview=1)
#oscope.get_curve_data()
#MDO3034.curve_to_xy()
