import serial
import time
from datetime import datetime
import csv
import tests.scratch as s
import instruments.MDO3034 as MDO3034

'''
Purpose: Automate the generation of a test matrix that runs 
'''

# Parameters
port = 'COM8'
baud = 2000000
start_frequency_i = 81000000  # hz
end_frequency_i = 85000000  # hz
step_i = 200000  # hz
start_frequency_s = 81000000  # hz
end_frequency_s = 85000000  # hz
step_s = 200000  # hz
step_delay = 1  # seconds
dtfmtstr = "%m%d%Y %H:%M:%S"

# Create output file with a timestamp
# TODO Follow data folder path for output
# TODO Split script reliance on 'scratch', ideally it only needs to mimick the cold mark testing
test_file_output = 'sweep_{}.csv'.format(datetime.strftime(datetime.now(), "%m%d%Y_%H_%M_%S"))

# Set up serial device
device = serial.Serial(port, baud, timeout=0.01)
print("Connecting via [{}]...".format(device.name))
print(device)


def send_get(command):
    device.write(command)
    result = device.readline()
    while result == b'':
        result = device.readline()
    return result.decode('ISO-8859-1').rstrip('\r\n')


def set_freq(freq_i, freq_s, return_in_mhz=False):
    result = send_get('set_freq_i {} '.format(freq_i).encode())
    # Allow the device time to respond
    while result is None:
        pass
    result = send_get('set_freq_s {} '.format(freq_s).encode())
    # Allow the device time to respond
    while result is None:
        pass
    curr_freq_i = send_get(b'get_freq_i ')
    curr_freq_s = send_get(b'get_freq_s ')

    if return_in_mhz:
        curr_freq_s = float(curr_freq_s) / 1000000.0
        curr_freq_i = float(curr_freq_i) / 1000000.0
    return curr_freq_i, curr_freq_s


def main():
    # Grab current settings and display them
    print("============= Current Device State =============")
    print("Time: {}".format(datetime.strftime(datetime.now(), dtfmtstr)))
    print("Model Number: {}".format(send_get(b'get_model_number_full ')))
    print("Serial Number: {}".format(send_get(b'get_serial_number ')))
    print("Steady State Frequency: {}".format(send_get(b'get_freq_s ')))
    print("Initial Frequency: {}".format(send_get(b'get_freq_i ')))
    print("Tickle Length: {}".format(send_get(b'get_tickle_len ')))
    print("Temperature: {}".format(send_get(b'get_temp ')))
    print("Voltage: {}".format(send_get(b'get_voltage ')))
    print("Current: {}".format(send_get(b'get_current ')))

    # Create header row of csv
    with open(test_file_output, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        header = ['steady freq->']
        test_freq_s = start_frequency_s

        # Create list of frequencies
        while test_freq_s <= end_frequency_s:
            header.append(test_freq_s)
            test_freq_s += step_s

        # Add header
        csvwriter.writerow(header)

    # Run through the frequency sweep
    test_freq_i = start_frequency_i
    while test_freq_i <= end_frequency_i:
        data = ['{}'.format(test_freq_i)]
        test_freq_s = start_frequency_s
        while test_freq_s <= end_frequency_s:
            # Set frequency
            freq_state = set_freq(test_freq_i, test_freq_s, return_in_mhz=True)
            print("###################### Frequencies set to: [Initial: {}MHz] [Steady: {}MHz] ######################".format(freq_state[0], freq_state[1]))

            # Give user prompt and wait for data input #TODO Eventually make this run the required test automatically
            # result = input("Result/Notes for test at cell {} | {}: ".format(freq_state[0], freq_state[1]))
            # data.append(result)

            # Automatic Test Run and waveform data collection
            #s.main()
            '''
            scope = MDO3034.MDO3034(reset=1)
            scope.cursor_function('WAVEform')
            scope.set_cursor_position('4.5E-2')
            result = scope.get_cursor_hval()
            data.append(result)
            '''

            # Step freq
            test_freq_s += step_s
        # Step freq
        test_freq_i += step_i

        # Add data to csv
        with open(test_file_output, 'a+', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(data)


if __name__ == "__main__":
    main()
