import pathlib
import pandas as pd
import sys
from instruments import Firestar, AG33511b
import time
import os
from datetime import datetime

def laser_states(test_info, cfg_file = 'default.csv', cfg_dir = 'tests\\lst_laser_states\\configs', phase='debug', dut='poc1'):
    # Parse configuration file #####################################################
    base_dir = [i for i in sys.path if 'testbench' in i][1]
    cfg = pd.read_csv(base_dir + '\\' + cfg_dir + '\\' + cfg_file)
    data_dir = test_info[0]
    phase = test_info[1]
    dut = test_info[2]

    # Create results directories
    save_dir_stub = data_dir + '\\' + phase + '\\lst_laser_states\\'
    pathlib.Path(save_dir_stub + 'res').mkdir(parents=True, exist_ok=True)
    date_str = datetime.today().strftime('%Y_%m_%d_%H_%M_%S')

    # Connect to instruments #######################################################
    awg = AG33511b.AG33511b(reset=1, verbose=1)
    awg.write(":SOURCE:DATA:VOLATILE:CLEAR")
    fs = Firestar.Firestar()

    res_dir = save_dir_stub + 'res\\'
    res_name = dut + '_laser_states_%s.csv' % (date_str)
    f=open(res_dir + res_name,'w')

    header_str = 'REMOTE_INTERLOCK_REQUEST,REMOTE_RESET_REQUEST,SHUTTER_OPEN_REQUEST,PWM,'
    header_str = header_str + 'EXP_VDD,EXP_TICKLE,'
    header_str = header_str + 'EXP_INTERLOCK_OPEN_OUTPUT,EXP_FAULT_DETECTED_OUTPUT,EXP_LASER_READY_OUTPUT,EXP_SHUTTER_OPEN_OUTPUT,'
    header_str = header_str + 'EXP_INT,EXP_TMP,EXP_RDY,EXP_SHT,EXP_LASE,'
    header_str = header_str + 'VDD,TICKLE,'
    header_str = header_str + 'INTERLOCK_OPEN_OUTPUT,FAULT_DETECTED_OUTPUT,LASER_READY_OUTPUT,SHUTTER_OPEN_OUTPUT,'
    header_str = header_str + 'INT,TMP,RDY,SHT,LASE,PASS/FAIL\n'
    
    f.write(header_str)
    
    for index, row in cfg.iterrows():
        inputs = (int(row['REMOTE_INTERLOCK_REQUEST']),int(row['REMOTE_RESET_REQUEST']),int(row['SHUTTER_OPEN_REQUEST']),int(row['PWM']))
        outputs = fs.read()

        exp_inputs = inputs
        exp_outputs = outputs
        exp_rf_dc = 'x'
        exp_tickle = 'x'
        exp_leds = 'xxxxx'
        
        fs.write(Firestar.REMOTE_INTERLOCK_REQUEST,inputs[0])
        fs.write(Firestar.REMOTE_RESET_REQUEST,inputs[1])
        fs.write(Firestar.SHUTTER_OPEN_REQUEST,inputs[2])
        print(fs.print_inputs2())
        if int(row['PWM']):
            time.sleep(2)
            awg.set_continuous_pwm()
            awg.set_output('ON')
        rf_dc = (input('Is 48V present (y/n)?\n') or 'n')
        tickle = (input('Is tickle present(y/n)?\n') or 'n')
        leds = (input('Please input LED pattern:') or 'xxxxx')
        write_str = '%i,%i,%i,%i,' % (inputs[0], inputs[1], inputs[2],inputs[3])
        write_str = write_str + '%s,%s,' % (exp_rf_dc, exp_tickle)
        write_str = write_str + '%s,%s,%s,%s,' % (exp_outputs[0],exp_outputs[1],exp_outputs[2],exp_outputs[3])
        write_str = write_str + '%s,%s,%s,%s,%s,' % (exp_leds[0],exp_leds[1],exp_leds[2],exp_leds[3],exp_leds[4])
        write_str = write_str + '%s,%s,' % (rf_dc, tickle)
        write_str = write_str + '%s,%s,%s,%s,' % (outputs[0],outputs[1],outputs[2],outputs[3])
        write_str = write_str + '%s,%s,%s,%s,%s\n' % (leds[0],leds[1],leds[2],leds[3],leds[4])
        #write_str = '%i,%i,%i,%i,%s,%s,%s,%s,%s,%s,%s\n' % (inputs[0], inputs[1], inputs[2],inputs[3], rf_dc, tickle, leds[0],leds[1],leds[2],leds[3],leds[4] )
        f.write(write_str)
        awg.set_output('OFF')

    f.close()


phase = 'debug'
dut = 'test_unit'
data_dir = 'C:\\Users\\aaron.scott\\Development\\testbench\\data'

pathlib.Path(data_dir + '\\' + phase).mkdir(parents=True, exist_ok=True)
test_info = (data_dir, phase, dut)

laser_states(test_info,'laser_states.csv')
