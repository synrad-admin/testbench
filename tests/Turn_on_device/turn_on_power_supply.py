# Author: Brendan Roozen - Research & Development Team:Synrad
# Date: 16 July 2021
# File Description: Script that turns on the Power, sets the paramters
# of the current and voltage supply.
# Instantiated within scratch.py-file that executes the the various scripts

#import modules
from instruments import BK9115
import configparser

# Function Definition:
# Called in scratch.py- this function allows the user to turn
# on the power supply remotely
# and set the appropriate voltage and current parameters without
# manually setting on the device
# Parameters: A configuration file and directory-
# found in Tests\Turn_on_device\configs
# Outputs: A text file with the current and voltage level of the
# power supply in it. 

def turn_on (cfg_file, cfg_dir = 'Turn_on_device/configs'):
             #Parse the configuration file
             config = configparser.ConfigParser() # creates the object to actually parse the file
             config.read(cfg_dir + '/' + cfg_file)

             #Parse the values here from the configuration file
             CURRENT = float(config['turn_power_on']['CURRENT'])
             VOLTAGE = float(config['turn_power_on']['VOLTAGE'])

             #Creates an object
             ps = BK9115.BK9115()

             #turn on power supply
             ps.set_output_state("ON")

             #Set the voltage and current level
             ps.set_voltage(VOLTAGE)
             f.write(ps.set_voltage(VOLTAGE))
             ps.set_current(CURRENT)
             f.write(ps.set_current(CURRENT))

             #Get the current and voltage values
             current = ps.get_current()
             voltage = ps.get_voltage()

             #Record current and voltage into power_supply_test.txt file
             f = open("power_supply_test.txt", "w")
             f.write("Current: \t%f\n" % current + " Amps(A)")
             f.write("Voltage: \t%f\n" % voltage + " Volts(V)")
