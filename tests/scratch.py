import time  # allows you to work with time related functions
import pathlib  # allows the user to create particular path files
from tests.lst_rise_fall.lst_rise_fall import rise_fall  # this just imports the object rise_fall subclass

from tests.Turn_on_device.turn_on_power_supply import turn_on
from tests.lst_cold_mark.lst_cold_mark import cold_mark

from tests.lst_power_linearity.lst_power_linearity import power_linearity
from tests.lst_power_stability.lst_power_stability import power_stability
from tests.lst_modulation.lst_modulation import modulation


# main function- executes the appropriate test- have to comment out a particular testS
def main():
    phase = 'Ti100_POC_DEBUG_12_17_2021'  # basic strings to use as a placeholder value
    dut = 'Ti100F_POC_3p25us_tickle_85p0_85p0'
    data_dir = 'C:\\Development\\testbench\\data'  # file path the test will use

    # Create Test Information, Save Directories, etc.
    pathlib.Path(data_dir + '\\' + phase).mkdir(parents=True, exist_ok=True)  # makes the directory
    test_info = (data_dir, phase, dut)  # test_info is just a parameter- one variable that encompasses three variables

    ############################################################################
    # BEGIN TESTS- in the development folder: each different test has its respective cycle
    ############################################################################
    # turn_on('turn_power_on.cfg')
    # time.sleep(10)
    # rise_fall(test_info,'rf_1khz_10percent.cfg') #includes the parameter and the appropriate configuration file
    # needed to run the test
    # rise_fall(test_info,'rf_100hz_10percent.cfg') #DONT RUN THIS ONE

    # Cold Mark Testing
    # cold_mark(test_info,'cm_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    cold_mark(test_info, 'cm_20kHz_10percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    # cold_mark(test_info,'cm_20kHz_20percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    # cold_mark(test_info,'cm_20kHz_30percent_166Hz_83percent.cfg', phase=phase, dut=dut)
    # cold_mark(test_info,'cm_20kHz_40percent_166Hz_83percent.cfg', phase=phase, dut=dut)

    # Marking Quality Testing

    # Power Linearity Testing
    # power_linearity(test_info,'pl_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)

    # Modulation Testing
    # modulation(test_info,'mod_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)

    # Peak Power Testing

    # Power over time testing
    # time.sleep(10)
    # power_stability(test_info,'ps_20kHz_07percent_166Hz_83percent.cfg', phase=phase, dut=dut)


if __name__ == "__main__":
    main()
