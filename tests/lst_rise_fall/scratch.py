import tests.lst_rise_fall.lst_rise_fall as lstrf
import time

phase = 'debug'
dut = 'alpha1'
test_cases = ('lst_rise_fall_1khz_10percent.cfg',)

for tc in test_cases:
    lstrf.rise_fall(tc, phase=phase, dut=dut)

