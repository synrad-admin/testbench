from instruments import AG33511b, MDO3034
import time
from scipy.signal import savgol_filter
from matplotlib import pyplot as plt
import configparser
import pathlib
from datetime import datetime
import pandas as pd
 
def rise_fall(test_info, cfg_file, cfg_dir = 'lst_rise_fall/configs'):
    # Parse configuration file #####################################################
    config = configparser.ConfigParser()
    config.read(cfg_dir + '/' + cfg_file) 

    #these are located in the configuration (.cfg) in the directory
    STIMULUS_FREQUENCY = float(config['default']['STIMULUS_FREQUENCY'])
    STIMULUS_DUTY_CYCLE = float(config['default']['STIMULUS_DUTY_CYCLE'])
    NCYCLES = int(config['default']['NCYCLES'])

    HORIZONTAL_SCALE = float(config['default']['HORIZONTAL_SCALE'])
    HORIZONTAL_DELAY = float(config['default']['HORIZONTAL_DELAY'])
    HORIZONTAL_RECORD_LENGTH = float(config['default']['HORIZONTAL_RECORD_LENGTH'])
    
    TRIGGER_CHANNEL = int(config['default']['TRIGGER_CHANNEL'])
    TRIGGER_SCALE = float(config['default']['TRIGGER_SCALE'])
    TRIGGER_LEVEL = float(config['default']['TRIGGER_LEVEL'])
    TRIGGER_POSITION = float(config['default']['TRIGGER_POSITION'])
    
    FAST_DETECTOR_CHANNEL = int(config['default']['FAST_DETECTOR_CHANNEL'])
    FAST_DETECTOR_SCALE = float(config['default']['FAST_DETECTOR_SCALE'])
    FAST_DETECTOR_POSITION = float(config['default']['FAST_DETECTOR_POSITION'])
    FAST_DETECTOR_BANDWIDTH = float(config['default']['FAST_DETECTOR_BANDWIDTH'])
    FAST_DETECTOR_TERMINATION = config['default']['FAST_DETECTOR_TERMINATION']

    #test_info = (data_dir = data_dir = 'C:\\Development\\testbench\\data', phase = 'debug', dut = 'test_unit')
    data_dir = test_info[0]
    phase = test_info[1]
    dut = test_info[2]

    # Create results directories
    save_dir_stub = data_dir + '\\' + phase + '\\lst_rise_fall\\'
    pathlib.Path(save_dir_stub + 'pngs').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'raw').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'res').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'figs').mkdir(parents=True, exist_ok=True)
    date_str = datetime.today().strftime('%Y_%m_%d_%H_%M_%S')
    
    # Connect to instruments ####################################################### Creating the objects
    awg = AG33511b.AG33511b(reset=1, verbose=1)
    awg.write(":SOURCE:DATA:VOLATILE:CLEAR")
    oscope = MDO3034.MDO3034(reset=1) #set the instrument to the actual device

    # Configure oscilloscope #######################################################
    # Configure timebase
    oscope.set_horizontal_scale(HORIZONTAL_SCALE)
    oscope.set_horizontal_delay_time(HORIZONTAL_DELAY)
    oscope.set_horizontal_record_length(HORIZONTAL_RECORD_LENGTH)
    
    # Configure trigger channel 
    oscope.display_channel(TRIGGER_CHANNEL, "ON")
    oscope.set_ch_scale(TRIGGER_CHANNEL, TRIGGER_SCALE)
    oscope.set_normal_edge_trigger(TRIGGER_CHANNEL, TRIGGER_LEVEL)
    oscope.set_ch_position(TRIGGER_CHANNEL, TRIGGER_POSITION)

    # Configure fast detector channel
    oscope.display_channel(FAST_DETECTOR_CHANNEL, "ON")
    oscope.set_ch_termination(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_TERMINATION)
    oscope.set_ch_position(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_POSITION)
    oscope.set_ch_scale(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_SCALE)
    oscope.set_ch_bandwidth(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_BANDWIDTH)
    #oscope.set_single_shot()

    # Configure function generator #################################################
    oscope.set_single_shot()
    awg.set_pulse_burst(freq=STIMULUS_FREQUENCY, duty=STIMULUS_DUTY_CYCLE, cycles = NCYCLES)
    time.sleep(.5)
    oscope.set_single_shot()
    awg.write("OUTPUT OFF")

    # Execute test #################################################################
    #awg.set_continuous_pwm()
    #oscope.display_channel(4, "OFF") 

    # Process data #################################################################
    png_dir = save_dir_stub + 'pngs\\'
    png_name = dut + '_rise_fall_%ihz_%ipc_%s.png' % (int(STIMULUS_FREQUENCY), int(STIMULUS_DUTY_CYCLE*100), date_str)
    oscope.save_screen_image(preview=0, save_dir=png_dir, save_name=png_name)

    raw_dir = save_dir_stub + 'raw\\'
    raw_name = dut + '_rise_fall_%ihz_%ipc_%s.csv' % (int(STIMULUS_FREQUENCY), int(STIMULUS_DUTY_CYCLE*100), date_str)
    oscope.get_curve_data(stop=100e3)
    x, y = MDO3034.curve_to_xy(preview=0, save_dir=raw_dir, save_name=raw_name)

    fig_dir = save_dir_stub + 'figs\\'
    fig_name = dut + '_rise_fall_%s_%s_%s_PROCESSED.png' % (str(STIMULUS_FREQUENCY), str(STIMULUS_DUTY_CYCLE), date_str)

    res_dir = save_dir_stub + 'res\\'
    res_name = dut + '_rise_fall_%s_%s_%s_PROCESSED.txt' % (str(STIMULUS_FREQUENCY), str(STIMULUS_DUTY_CYCLE), date_str)

    rise_fall_process(raw_dir, raw_name, fig_dir, fig_name, res_dir, res_name, config)

    
    """
    y_flt = savgol_filter(y, 501, 2)
    plt.plot(y_flt)
    plt.show()
    """

def rise_fall_process(raw_dir, raw_name, fig_dir, fig_name, res_dir, res_name, config):

    filename = raw_dir + "\\" + raw_name
    
    #HORIZONTAL_SCALE = 10e-3
    #HORIZONTAL_DELAY = 40e-3
    #HORIZONTAL_RECORD_LENGTH = 1e5

    #Extracting values from configuration files and setting them equal to new variables
    HORIZONTAL_SCALE = float(config['default']['HORIZONTAL_SCALE'])
    HORIZONTAL_DELAY = float(config['default']['HORIZONTAL_DELAY'])
    HORIZONTAL_RECORD_LENGTH = float(config['default']['HORIZONTAL_RECORD_LENGTH'])

    STIMULUS_FREQUENCY = float(config['default']['STIMULUS_FREQUENCY'])
    STIMULUS_DUTY_CYCLE = float(config['default']['STIMULUS_DUTY_CYCLE'])

    t_inc = HORIZONTAL_SCALE*10/HORIZONTAL_RECORD_LENGTH
    
    plt.style.use('bmh')
    fig = plt.figure(constrained_layout=True)#if true and figure already exists, then the figure is cleared
    fig.set_size_inches(8, 4) #sets the width and height of the figure in inches
    ax0=plt.gca() #feature for the axis

    
    rf_data = pd.read_csv(filename, delimiter = ',', names = ['x','y']) #reading the file name
    ax0.plot(rf_data['x']*1e6,rf_data['y'])
    y_min = min(rf_data['y']) #discerns the minimum value
    y_max = max(rf_data['y']) #discerns the maximum value

    # Find turn on time
    t0 = HORIZONTAL_SCALE*10/2-HORIZONTAL_DELAY
    ax0.plot(t0*1e6, 0,'*r', markersize=12)

    # Find firing delay time
    #threshold_on = max(rf_data['y'])*.05
    #t1_ind = next(x for x, val in enumerate(rf_data['y'])if val > threshold_on)
    #t1 = rf_data['x'][t1_ind]
    #t_firing_delay = t1 - t0
    #ax0.plot(t1*1e6, 0,'*r', markersize=12)
    
    ##### Want to be able to print all this important information ####
    # Find turn off time
    t2 = t0+1/STIMULUS_FREQUENCY*STIMULUS_DUTY_CYCLE
    t2_ind = int(t2/t_inc)
    t2_pow = rf_data['y'][t2_ind]
    ax0.plot(t2*1e6, t2_pow,'*r', markersize=12)

    # Find t_rise_10
    threshold_10 = t2_pow *.1
    t_rise_10_ind = next(x for x, val in enumerate(rf_data['y'])if val > threshold_10)
    t_rise_10 = rf_data['x'][t_rise_10_ind]
    ax0.plot(t_rise_10*1e6, threshold_10,'*r', markersize=12)

    # Find t_rise_90
    threshold_90 = t2_pow *.9
    t_rise_90_ind = next(x for x, val in enumerate(rf_data['y'])if val > threshold_90)
    t_rise_90 = rf_data['x'][t_rise_90_ind]
    ax0.plot(t_rise_90*1e6, threshold_90,'*r', markersize=12)

    # Find t_fall_90
    t_fall_90_ind = next(x for x, val in enumerate(rf_data['y'][t2_ind:])if val < threshold_90) + t2_ind
    t_fall_90 = rf_data['x'][t_fall_90_ind]
    ax0.plot(t_fall_90*1e6, threshold_90,'*r', markersize=12)

    # Find t_fall 10
    t_fall_10_ind = next(x for x, val in enumerate(rf_data['y'][t2_ind:])if val < threshold_10) + t2_ind
    t_fall_10 = rf_data['x'][t_fall_10_ind]
    ax0.plot(t_fall_10*1e6, threshold_10,'*r', markersize=12)

    # Calculation of the firing delay
    t_rise = int((t_rise_90-t_rise_10)*1e6)
    t_fall = int((t_fall_10-t_fall_90)*1e6)
    t_firing_delay = int((t_rise_10-t0)*1e6)

    print(res_dir + res_name)

    f = open(res_dir + res_name, 'w')
    f.write("t_rise = %i us\n" % t_rise)
    f.write("t_fall = %i us\n" % t_fall)
    f.write("t_firing_delay = %i us\n" % t_firing_delay)
    f.write("v_achieved = %f V\n" % t2_pow)
    f.close()
    
    XMIN = HORIZONTAL_SCALE*1e6-1e3
    XMAX = XMIN+(1/166)*1e6*10+2e3
    #ax0.set_xlim((XMIN, XMAX))
    ax0.set_xlabel('Time (us)')
    ax0.set_ylabel('Amplitude (V)')
    
    suptitle_str = filename.split('\\')[-1][:-4]
    title_str = 't_rise = %ius, t_fall = %ius' % (t_rise, t_fall)
    
    plt.suptitle(suptitle_str, fontsize=14)
    plt.title(title_str, fontsize=10)

    plt.savefig(fig_dir + fig_name)
    plt.show()
    plt.close()


    

    
