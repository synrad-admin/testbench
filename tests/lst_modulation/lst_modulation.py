from instruments import AG33511b, BK9115, SmartHead
import time
from scipy.signal import savgol_filter
from matplotlib import pyplot as plt
import configparser
import pathlib
from datetime import datetime
from multiprocessing import Process, Queue
from utilities.DataLogger import log_data
import pandas as pd

def modulation(test_info, cfg_file = 'default.cfg', cfg_dir = 'lst_modulation/configs', phase='debug', dut='poc1'):
    # Parse configuration file #####################################################
    config = configparser.ConfigParser()
    config.read(cfg_dir + '/' + cfg_file)

    STIMULUS_FILENAME = config['default']['STIMULUS_FILENAME']
    STIMULUS_FILEDIR = config['default']['STIMULUS_FILEDIR']

    data_dir = test_info[0]
    phase = test_info[1]
    dut = test_info[2]

    # Create results directories
    save_dir_stub = data_dir + '\\' + phase + '\\lst_modulation\\'
    pathlib.Path(save_dir_stub + 'raw').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'res').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'figs').mkdir(parents=True, exist_ok=True)
    date_str = datetime.today().strftime('%Y_%m_%d_%H_%M_%S')
    #save_name = save_dir_stub + 'raw\\test.csv' 
    
    # Connect to instruments #######################################################
    awg = AG33511b.AG33511b(reset=1, verbose=1)
    awg.write(":SOURCE:DATA:VOLATILE:CLEAR")

    #ps = BK9115.BK9115()
    #sh = SmartHead.SmartHead()

    raw_dir = save_dir_stub + 'raw\\'
    raw_name = dut + '_modulation_%s.csv' % (date_str)

    q = Queue()
    p = Process(target=log_data, args=(3600,raw_dir, raw_name, q))
    p.start()

    awg.set_continuous_pwm()
    awg.set_output("ON")
    for dcycle in (10, 50, 90):
        for frequency in range(1000,100000,1000):
            awg.set_pwm_freq_dc(frequency, dcycle)
            time.sleep(2)
    q.put("STOP")
    awg.set_output("OFF")
    p.join()
    p.close()

    fig_dir = save_dir_stub + 'figs\\'
    fig_name = dut + '_modulation_%s_PROCESSED.png' % (date_str)
    modulation_post_process(raw_dir, raw_name, fig_dir, fig_name)


def modulation_post_process(raw_dir, raw_name, fig_dir = '', fig_name=''):

    filename = raw_dir + "\\" + raw_name
    
    #filename = 'C:\\Users\\aaron.scott\\Development\\testbench\\data\\debug\\lst_modulation\\raw\\test_unit_modulation_2021_06_21_15_55_55.csv'

    plt.style.use('bmh')

    pl_data = pd.read_csv(filename, delimiter = ',')

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8, 4)
    ax0=plt.gca()
    
    ax0.plot(pl_data['time'],pl_data['Power (W)'])

    ax0.set_xlabel('Time (s)')
    ax0.set_ylabel('Power (W)')
    ax0.title.set_text('Modulation')

    ax1 = ax0.twinx()
    ax1.set_ylabel('Current (A)')
    ax1.plot(pl_data['time'],pl_data['Current(A)'], '--')

    suptitle_str = filename.split('\\')[-1][:-4]

    plt.suptitle(suptitle_str, fontsize=14)
    plt.savefig(fig_dir + fig_name)
    plt.close()
        
    plt.show()
    
