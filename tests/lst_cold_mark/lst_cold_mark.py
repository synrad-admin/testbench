from instruments import AG33511b, MDO3034
import time
from scipy.signal import savgol_filter
from matplotlib import pyplot as plt
import configparser
import pathlib
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter
import sys
import pandas as pd


def cold_mark(test_info, cfg_file='default.cfg', cfg_dir='tests\\lst_cold_mark\\configs', phase='debug', dut='poc1'):
    # Parse configuration file #####################################################
    base_dir = [i for i in sys.path if 'testbench' in i][1]
    config = configparser.ConfigParser()
    cfg_fqdn = base_dir + '\\' + cfg_dir + '\\' + cfg_file
    config.read(base_dir + '\\' + cfg_dir + '\\' + cfg_file)

    STIMULUS_FILENAME = config['default']['STIMULUS_FILENAME']
    STIMULUS_FILEDIR = config['default']['STIMULUS_FILEDIR']

    PWM_FREQUENCY = config['default']['PWM_FREQUENCY']
    PWM_DUTY_CYCLE = config['default']['PWM_DUTY_CYCLE']

    GATE_FREQUENCY = config['default']['GATE_FREQUENCY']
    GATE_DUTY_CYCLE = config['default']['GATE_DUTY_CYCLE']

    HORIZONTAL_SCALE = float(config['default']['HORIZONTAL_SCALE'])
    HORIZONTAL_DELAY = float(config['default']['HORIZONTAL_DELAY'])
    HORIZONTAL_RECORD_LENGTH = float(config['default']['HORIZONTAL_RECORD_LENGTH'])

    TRIGGER_CHANNEL = int(config['default']['TRIGGER_CHANNEL'])
    TRIGGER_SCALE = float(config['default']['TRIGGER_SCALE'])
    TRIGGER_LEVEL = float(config['default']['TRIGGER_LEVEL'])
    TRIGGER_POSITION = float(config['default']['TRIGGER_POSITION'])

    FAST_DETECTOR_CHANNEL = int(config['default']['FAST_DETECTOR_CHANNEL'])
    FAST_DETECTOR_SCALE = float(config['default']['FAST_DETECTOR_SCALE'])
    FAST_DETECTOR_POSITION = float(config['default']['FAST_DETECTOR_POSITION'])
    FAST_DETECTOR_BANDWIDTH = float(config['default']['FAST_DETECTOR_BANDWIDTH'])
    FAST_DETECTOR_TERMINATION = config['default']['FAST_DETECTOR_TERMINATION']

    data_dir = test_info[0]
    phase = test_info[1]
    dut = test_info[2]

    # Create results directories
    save_dir_stub = data_dir + '\\' + phase + '\\lst_cold_mark\\'
    pathlib.Path(save_dir_stub + 'pngs').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'raw').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'res').mkdir(parents=True, exist_ok=True)
    pathlib.Path(save_dir_stub + 'figs').mkdir(parents=True, exist_ok=True)
    date_str = datetime.today().strftime('%Y_%m_%d_%H_%M_%S')

    # Connect to instruments #######################################################
    awg = AG33511b.AG33511b(reset=1, verbose=1)
    awg.write(":SOURCE:DATA:VOLATILE:CLEAR")
    oscope = MDO3034.MDO3034(reset=1)

    # Configure oscilloscope #######################################################
    # Configure timebase
    oscope.set_horizontal_scale(HORIZONTAL_SCALE)
    oscope.set_horizontal_delay_time(HORIZONTAL_DELAY)
    oscope.set_horizontal_record_length(HORIZONTAL_RECORD_LENGTH)

    # Configure trigger channel 
    oscope.display_channel(TRIGGER_CHANNEL, "ON")
    oscope.set_ch_scale(TRIGGER_CHANNEL, TRIGGER_SCALE)
    oscope.set_normal_edge_trigger(TRIGGER_CHANNEL, TRIGGER_LEVEL)
    oscope.set_ch_position(TRIGGER_CHANNEL, TRIGGER_POSITION)

    # Configure fast detector channel
    oscope.display_channel(FAST_DETECTOR_CHANNEL, "ON")
    oscope.set_ch_termination(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_TERMINATION)
    oscope.set_ch_position(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_POSITION)
    oscope.set_ch_scale(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_SCALE)
    oscope.set_ch_bandwidth(FAST_DETECTOR_CHANNEL, FAST_DETECTOR_BANDWIDTH)
    # oscope.set_single_shot()

    # Configure function generator #################################################
    f = open(STIMULUS_FILEDIR + '/' + STIMULUS_FILENAME, 'r')
    dat_str = f.read()[:-1]
    f.close()
    awg.set_arb_wf(data_str=dat_str)
    time.sleep(.2)

    # Execute test #################################################################
    # awg.set_continuous_pwm()

    # oscope.display_channel(4, "OFF")

    # Process data #################################################################
    # oscope.save_screen_image(preview=0)
    png_dir = save_dir_stub + 'pngs\\'
    png_name = dut + '_cold_mark_%s_%s_%s.png' % (PWM_FREQUENCY, PWM_DUTY_CYCLE, date_str)
    oscope.save_screen_image(preview=0, save_dir=png_dir, save_name=png_name)

    raw_dir = save_dir_stub + 'raw\\'
    raw_name = dut + '_cold_mark_%s_%s_%s.csv' % (PWM_FREQUENCY, PWM_DUTY_CYCLE, date_str)
    oscope.get_curve_data(stop=100e3)
    x, y = MDO3034.curve_to_xy(preview=0, save_dir=raw_dir, save_name=raw_name)

    fig_dir = save_dir_stub + 'figs\\'
    fig_name = dut + '_cold_mark_%s_%s_%s_PROCESSED.png' % (PWM_FREQUENCY, PWM_DUTY_CYCLE, date_str)
    cold_mark_post_process(raw_dir, raw_name, fig_dir, fig_name)

    """
    oscope.get_curve_data(stop=100e3)
    x, y = MDO3034.curve_to_xy(preview=0)
    y_flt = savgol_filter(y, 501, 2)
    plt.plot(y_flt)
    plt.show()
    """


def cold_mark_post_process(raw_dir, raw_name, fig_dir='', fig_name=''):
    filename = raw_dir + "\\" + raw_name

    HORIZONTAL_SCALE = 10e-3
    HORIZONTAL_DELAY = 40e-3
    HORIZONTAL_RECORD_LENGTH = 1e5

    plt.style.use('bmh')

    cm_data = pd.read_csv(filename, delimiter=',', names=['x', 'y'])
    cm_smoothed = savgol_filter(cm_data['y'], 301, 3)

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8, 8)

    gs = fig.add_gridspec(2, 2)
    ax0 = fig.add_subplot(gs[0, :])
    ax1 = fig.add_subplot(gs[1, 0])
    ax2 = fig.add_subplot(gs[1, 1])

    ax0.plot(cm_data['x'] * 1e6, cm_data['y'])
    ax0.plot(cm_data['x'] * 1e6, cm_smoothed)
    XMIN = HORIZONTAL_SCALE * 1e6 - 1e3
    XMAX = XMIN + (1 / 166) * 1e6 * 10 + 2e3
    ax0.set_xlim((XMIN, XMAX))
    ax0.set_xlabel('Time (us)')
    ax0.set_ylabel('Amplitude (V)')
    ax0.title.set_text('Pulse Train')

    ax1.plot(cm_data['x'] * 1e6, cm_data['y'])
    ax1.plot(cm_data['x'] * 1e6, cm_smoothed)
    XMIN = HORIZONTAL_SCALE * 1e6 - 100
    XMAX = XMIN + (1 / 166) * 1e6 * .1
    ax1.set_xlim((XMIN, XMAX))
    ax1.set_xlabel('Time (us)')
    ax1.set_ylabel('Amplitude (V)')
    ax1.title.set_text('First Pulse')

    ax2.plot(cm_data['x'] * 1e6, cm_data['y'])
    ax2.plot(cm_data['x'] * 1e6, cm_smoothed)
    XMIN = XMIN + 6024
    XMAX = XMAX + 6024
    ax2.set_xlim((XMIN, XMAX))
    ax2.set_xlabel('Time (us)')
    ax2.set_ylabel('Amplitude (V)')
    ax2.title.set_text('Second Pulse')

    suptitle_str = filename.split('\\')[-1][:-4]
    # title_str = 't_rise_1 = %ius, t_rise_2 = %ius, pulse_diff = %i' % (100, 200, 50)

    plt.suptitle(suptitle_str, fontsize=14)
    # plt.title(title_str, fontsize=10)

    plt.savefig(fig_dir + fig_name)
    # plt.show()
    plt.close()

# rn = 'test_unit_cold_mark_20kHz_20pc_2021_06_18_13_13_46.csv'
# rd = 'C:\\Users\\aaron.scott\\Development\\testbench\\data\\debug\\lst_cold_mark\\raw\\'

# fn = 'test_unit_cold_mark_20kHz_20pc_2021_06_18_13_13_46_PROCESSED.png'
# fd = 'C:\\Users\\aaron.scott\\Development\\testbench\\data\\debug\\lst_cold_mark\\figs\\'

# cold_mark_post_process(rd, rn, fd, fn)
