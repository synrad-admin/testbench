import tests.lst_cold_mark.lst_cold_mark as lstcm
import time

phase = 'debug'
dut = 'alpha1'
test_cases = ('mqt_20kHz_07percent_166Hz_83percent.cfg',
              'mqt_20kHz_10percent_166Hz_83percent.cfg',
              'mqt_20kHz_20percent_166Hz_83percent.cfg',
              'mqt_20kHz_30percent_166Hz_83percent.cfg',
              'mqt_20kHz_40percent_166Hz_83percent.cfg',
              'mqt_20kHz_50percent_166Hz_83percent.cfg')

for tc in test_cases:
    lstcm.cold_mark(tc, phase=phase, dut=dut)

